﻿using System;
using System.Windows.Forms;

/*
 * Title: Confirm form
 * Author: Juckelfunk
 * Date: 03.06.2018 | 15:34
 * Desc: Ask the user a question and let him choose between up to three choices which will be the result
 */

namespace Wallpaper_Manager
{
    public partial class ConfirmForm : Form
    {
        public string Result { get; set; } // The result as the string that was the button text

        /// <summary>
        /// Ask the user a question and let him choose between two options
        /// </summary>
        /// <param name="text">The question to be asked</param>
        /// <param name="button1Text">Text of the first option</param>
        /// <param name="button2Text">Text of the second option</param>
        public ConfirmForm(string text, string button1Text, string button2Text)
        {
            InitializeComponent();
            
            textBox.Text = text.Replace("\n", Environment.NewLine);
            // There's a bug that selectes all text when set. We prevent this with selecting nothing here
            textBox.Select(0, 0);
            button1.Text = button1Text;
            button2.Text = button2Text;
            // Don't show the third button because we only have two options
            button3.Visible = false;
        }

        /// <summary>
        /// Ask the user a question and let him choose between three options
        /// </summary>
        /// <param name="text">The question to be asked</param>
        /// <param name="button1Text">Text of the first option</param>
        /// <param name="button2Text">Text of the second option</param>
        /// <param name="button3Text">Text of the third option</param>
        public ConfirmForm(string text, string button1Text, string button2Text, string button3Text)
        {
            InitializeComponent();

            textBox.Text = text.Replace("\n", Environment.NewLine);
            // There's a bug that selectes all text when set. We prevent this with selecting nothing here
            textBox.Select(0, 0);
            button1.Text = button1Text;
            button2.Text = button2Text;
            button3.Text = button3Text;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Result = button3.Text;
            Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Result = button2.Text;
            Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Result = button1.Text;
            Close();
        }
    }
}
