﻿using System;
using System.Drawing;
using System.Windows.Forms;

/*
 * Title: Show Image Form
 * Author: Juckelfunk
 * Date: 30.06.2018 | 15:40 CET
 * Desc: Give the user a fullscreen preview of how the wallpaper would look like with easy exit
 */

namespace Wallpaper_Manager
{
    public partial class ShowImageForm : Form
    {
        /// <summary>
        /// Show the user a fullscreen preview of a wallpaper according to the image style
        /// </summary>
        /// <param name="wallpaper">The wallpaper that will be shown</param>
        public ShowImageForm(Wallpaper wallpaper)
        {
            InitializeComponent();

            switch (wallpaper.ImageStyle)
            {
                case Wallpaper.Style.Stretched:
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    break;
                case Wallpaper.Style.Centered:
                    pictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                    break;
                case Wallpaper.Style.Tiled:
                    pictureBox.SizeMode = PictureBoxSizeMode.Normal;
                    break;
                case Wallpaper.Style.Fit:
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                    break;
                case Wallpaper.Style.Filled:
                    warnLabel.Visible = true;
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                    break;
            }

            try
            {
                pictureBox.Image = Image.FromFile(wallpaper.imagePath);
            }
            catch (Exception e)
            {
                Utils.ShowErrorMessage("Could not load image for image preview form: " + e);
                Close();
            }
        }

        private void CloseForm(object sender, EventArgs e)
        {
            Close();
        }

        private void ShowImageForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pictureBox.Image != null)
                pictureBox.Image.Dispose();
        }
    }
}
