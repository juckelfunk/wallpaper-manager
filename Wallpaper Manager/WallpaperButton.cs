﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

/*
 * Title: Wallpaper Button v2
 * Author: Juckelfunk
 * Date: 30.06.2018 | 12:38 CET
 * Desc: A button that represent a wallpaper with its most important information.
 *       This is a better version of a previous wallpaper button which only was a buttno with the style and file inforamtion as text
 */

namespace Wallpaper_Manager
{
    public partial class WallpaperButton : UserControl
    {
        public Wallpaper Wallpaper { get; set; }             // The wallpaper that is represented by this button
        public event EventHandler WallpaperButtonClickEvent; // Called when user clicks on any of the controls

        /// <summary>
        ///  Generate wallapaper button containing simple information about it with a preview
        /// </summary>
        /// <param name="wallpaper">The wallpaper that will be represented</param>
        public WallpaperButton(Wallpaper wallpaper)
        {
            InitializeComponent();

            Wallpaper = wallpaper;

            Image image = null;
            try
            {
                image = Image.FromFile(Wallpaper.imagePath);
            }
            catch (Exception e)
            {
                Program.Logger.Error("Could not load image for wallpaper button: " + wallpaper.imagePath + " : " + e.Message);
                foreach (Control l in Controls)
                {
                    if (l.GetType() == typeof(Label))
                        l.Text = "Could not load. More info in console!";
                }
                return;
            }

            // Load the preview if activated
            if (Settings.Default.Previews && image != null)
            {
                Bitmap bmp = Utils.ResizeImage(image, pictureBox.Width, pictureBox.Height);
                pictureBox.Image = bmp;
            }

            // File size label
            FileInfo info = new FileInfo(Wallpaper.imagePath);
            float size = info.Length / 1000f;
            if (size >= 1000)
                sizeLabel.Text = "Size: " + (size / 1000f).ToString("0.00") + " MB";
            else
                sizeLabel.Text = "Size: " + size.ToString("0.00") + " KB";

            // Image dimensions and file path labels
            dimensionLabel.Text = "Dimension: " + image.Width + "x" + image.Height;
            fileNameLabel.Text = "File: " + Path.GetFileName(Wallpaper.imagePath);

            // Style label
            switch (Wallpaper.ImageStyle)
            {
                case Wallpaper.Style.Stretched:
                    styleLable.Text = "Style: Streched";
                    break;
                case Wallpaper.Style.Centered:
                    styleLable.Text = "Style: Centered";
                    break;
                case Wallpaper.Style.Tiled:
                    styleLable.Text = "Style: Tiled";
                    break;
                case Wallpaper.Style.Fit:
                    styleLable.Text = "Style: Fit";
                    break;
                case Wallpaper.Style.Filled:
                    styleLable.Text = "Style: Filled";
                    break;
            }

            // TODO: Settings if we should dispose the image here. It is slower when we do it here but it requieres MUCH (!) more RAM
            image.Dispose();
        }

        public void DisposePreview()
        {
            if (pictureBox.Image != null)
                pictureBox.Image.Dispose();
        }
        
        /// <summary>
        /// The event that will be triggered if we click on this wallpaper.
        /// This is used to load the big preview image in the addWallpaperEvent form
        /// </summary>
        public void WallpaperButtonClick(object sender, EventArgs e)
        {
            WallpaperButtonClickEvent?.Invoke(this, EventArgs.Empty);
        }

        // Hover the wallpaperButton
        private void Hover(object sender, EventArgs e)
        {
            BackColor = SystemColors.ControlDarkDark;
            Cursor.Current = Cursors.Hand;
        }

        // Unhover the wallpaperButton
        private void UnHover(object sender, EventArgs e)
        {
            BackColor = SystemColors.ControlDark;
            Cursor.Current = Cursors.Arrow;
        }
    }
}
