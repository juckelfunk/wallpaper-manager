﻿namespace Wallpaper_Manager
{
    partial class WallpaperButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.styleLable = new System.Windows.Forms.Label();
            this.dimensionLabel = new System.Windows.Forms.Label();
            this.sizeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.MaximumSize = new System.Drawing.Size(96, 54);
            this.pictureBox.MinimumSize = new System.Drawing.Size(96, 54);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(96, 54);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.pictureBox.MouseLeave += new System.EventHandler(this.UnHover);
            this.pictureBox.MouseHover += new System.EventHandler(this.Hover);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(102, 0);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(29, 13);
            this.fileNameLabel.TabIndex = 1;
            this.fileNameLabel.Text = "File: ";
            this.fileNameLabel.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.fileNameLabel.MouseLeave += new System.EventHandler(this.UnHover);
            this.fileNameLabel.MouseHover += new System.EventHandler(this.Hover);
            // 
            // styleLable
            // 
            this.styleLable.AutoSize = true;
            this.styleLable.Location = new System.Drawing.Point(102, 13);
            this.styleLable.Name = "styleLable";
            this.styleLable.Size = new System.Drawing.Size(36, 13);
            this.styleLable.TabIndex = 2;
            this.styleLable.Text = "Style: ";
            this.styleLable.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.styleLable.MouseLeave += new System.EventHandler(this.UnHover);
            this.styleLable.MouseHover += new System.EventHandler(this.Hover);
            // 
            // dimensionLabel
            // 
            this.dimensionLabel.AutoSize = true;
            this.dimensionLabel.Location = new System.Drawing.Point(102, 26);
            this.dimensionLabel.Name = "dimensionLabel";
            this.dimensionLabel.Size = new System.Drawing.Size(62, 13);
            this.dimensionLabel.TabIndex = 3;
            this.dimensionLabel.Text = "Dimension: ";
            this.dimensionLabel.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.dimensionLabel.MouseLeave += new System.EventHandler(this.UnHover);
            this.dimensionLabel.MouseHover += new System.EventHandler(this.Hover);
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(102, 39);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(33, 13);
            this.sizeLabel.TabIndex = 4;
            this.sizeLabel.Text = "Size: ";
            this.sizeLabel.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.sizeLabel.MouseLeave += new System.EventHandler(this.UnHover);
            this.sizeLabel.MouseHover += new System.EventHandler(this.Hover);
            // 
            // WallpaperButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.sizeLabel);
            this.Controls.Add(this.dimensionLabel);
            this.Controls.Add(this.styleLable);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.pictureBox);
            this.Name = "WallpaperButton";
            this.Size = new System.Drawing.Size(330, 54);
            this.Click += new System.EventHandler(this.WallpaperButtonClick);
            this.MouseLeave += new System.EventHandler(this.UnHover);
            this.MouseHover += new System.EventHandler(this.Hover);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label fileNameLabel;
        public System.Windows.Forms.Label styleLable;
        private System.Windows.Forms.Label dimensionLabel;
        private System.Windows.Forms.Label sizeLabel;
    }
}
