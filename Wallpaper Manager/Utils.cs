﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Media;
using System.Runtime.Serialization;
using System.Windows.Forms;

/*
 * Title: Utils
 * Author: Juckelfunk and others
 * Date: 30.06.2018 | 12:57 CET
 * Desc: Collection of utils which would not fit in any other class. Also external classes which are from open source projects.
 *       The source link and more description are directly at the classes that were not written by me. Every source code that comes form
 *       anyone else is marked.
 */

namespace Wallpaper_Manager
{
    public static class Utils
    {
        /// <summary>
        /// Resize the image to the specified width and height.
        /// Source: https://stackoverflow.com/questions/1922040/resize-an-image-c-sharp, 30.06.2018, mpen
        /// Edited by me.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                //graphics.CompositingQuality = CompositingQuality.HighQuality;
                //graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //graphics.SmoothingMode = SmoothingMode.HighQuality;
                //graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.Low;
                graphics.SmoothingMode = SmoothingMode.HighSpeed;
                graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        /// <summary>
        /// Shows an error message in the console and as message box. Also plays a system error sound
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="caption">Headline for the message box</param>
        /// <param name="exit">Exit after message was shown</param>
        public static void ShowErrorMessage(string message, string caption = "An error occured - Wallpaper Manager", bool fatal = false, bool log = true)
        {
            // Make sure that wallpaper manager is in the caption so the user knows from where the error message is coming
            if (!caption.ToLower().Contains("manager") && !caption.Contains("-"))
                caption += " - Wallpaper Manager";

            // Play system error sound
            SystemSounds.Hand.Play();

            // Print the error message in the console with red foreground color
            if (fatal)
            {
                if (caption == "An error occured - Wallpapaer Manager")
                    caption = "AN FATAL ERROR OCCURED! - Wallpaper Manager";
                if (log)
                    Program.Logger.Fatal(message);
            }
            else
            {
                if (log)
                    Program.Logger.Error(message);
            }

            // Show the message box
            MessageBox.Show(
                message,
                caption,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        /// <summary>
        /// Shows the message of an exception in the console and as message box. Also plays a system error sound
        /// </summary>
        /// <param name="e">The exception</param>
        /// <param name="exit">Exit after message was shown</param>
        public static void ShowErrorMessage(Exception e, bool fatal = false)
        {
            ShowErrorMessage(e.Message, e.GetType().ToString(), fatal, false);
            if (fatal)
                Program.Logger.Fatal(e);
            else
                Program.Logger.Error(e);
        }

        /// <summary>
        /// Returns a string showing all current settings
        /// </summary>
        /// <returns>All settings as string niceley formated</returns>
        public static string SettingsToString()
        {
            string res = "";
            res += "Actual In Startup: \t\t" + Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized") + "\n";
            res += "Actual Settings In startup: \t" + Settings.Default.InStartup + "\n";
            res += "Message on startup: \t" + Settings.Default.MessageOnStartup + "\n";
            res += "Message on tray: \t\t" + Settings.Default.MesageOnTray + "\n";
            res += "Previews: \t\t" + Settings.Default.Previews + "\n";
            res += "Logging: \t\t\t" + Settings.Default.Logging + "\n";
            res += "Console on start: \t\t" + Settings.Default.ConsoleOnStart + "\n";
            res += "Start last event: \t\t" + Settings.Default.StartLastEvent + "\n";
            res += "Prefer event over time: \t" + Settings.Default.PreferEventOverTime + "\n";
            res += "Log path: \t\t" + Settings.Default.LogPath + "\n";
            res += "Current path: \t\t" + Settings.Default.CurrentPath + "\n";
            res += "Allow multiple instances: \t" + Settings.Default.AllowMultipleInstances + "\n";
            res += "Ask before closing: \t\t" + Settings.Default.AskBeforeClosing + "\n";
            return res;
        }
    }

    /// <summary>
    /// An exception thrown when there is an invalid line in a loading wallpaperEvents file
    /// </summary>
    class InvalidLineException : Exception
    {
        public InvalidLineException(string message)
            : base(message)
        { }
    }
}
