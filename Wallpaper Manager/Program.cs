﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

/**
 * Title: Juckelfunks Wallpaper Manager
 * Author: Juckelfunk
 * Date: 23.05.2018 | 22:01 CET
 * Desc: A simple wallpaper manager that allows you to change the wallpaper at a specified system time
 * BUG:
 *  - The event file is sometimes corrupted (I don't know why)
 *  - The user.config file was corrupted once
 *  - After adding a wallpaper event the garbage is not collected (even when manually done with CTRL+C)
 *  - After adding a new event the wallpaperEventButton is a few pixels left in the panel. After reloading it's fine again
 *  - When deactivating an wallpaper event which is triggered by an event other events that should be triggered but are after the deactivated event don't get started
 * TODO:
 *  - Replace boring select folder dialoge with a new cool one like in the image acceptor
 *  - Save automaticly after changes were made to an event
 *      (Save method into static Utils class so that one can call it from everywhere)
 *  - Sort events in time
 *  - Fade while setting the wallpaper (maybe with enableing gif as wallpaper (barnacules tutorial) and then generating a gif with fade
 *  - Multiple monitor support
 *  - Import events so that they are appended to the current event list
 *  - Clean up code (e.g. put the service stuff in own class, don't perform just button clicks on e.g. strip menu item clicks,
 *      put the code in a method and then call this method from button click and strip menu item)
 *  - More specific error output when failing to write to the registry
 *  - Check if time already exists and then dont let the event be generated
 *  - Update events after log in (events won't start if user is not logged in)
 *      I found this: https://stackoverflow.com/questions/12293286/how-to-get-windows-unlock-event-in-c-sharp-windows-application#
 *         using System;
 *         using Microsoft.Win32;
 *         
 *         // Based on: https://stackoverflow.com/a/604042/700926
 *         namespace WinLockMonitor
 *         {
 *             class Program
 *             {
 *                 static void Main(string[] args)
 *                 {
 *                     Microsoft.Win32.SystemEvents.SessionSwitch += new Microsoft.Win32.SessionSwitchEventHandler(SystemEvents_SessionSwitch);
 *                     Console.ReadLine();
 *                 }
 *         
 *                 static void SystemEvents_SessionSwitch(object sender, Microsoft.Win32.SessionSwitchEventArgs e)
 *                 {
 *                     if (e.Reason == SessionSwitchReason.SessionLock)
 *                     {
 *                         //I left my desk
 *                         Console.WriteLine("I left my desk");
 *                     }
 *                     else if (e.Reason == SessionSwitchReason.SessionUnlock)
 *                     {
 *                         //I returned to my desk
 *                         Console.WriteLine("I returned to my desk");
 *                     }
 *                 }
 *             }
 *         }
 */

namespace Wallpaper_Manager
{
    static class Program
    {
        // High DPI Stuff
        [DllImport("Shcore.dll")]
        static extern int SetProcessDpiAwareness(int PROCESS_DPI_AWARENESS);

        // According to https://msdn.microsoft.com/en-us/library/windows/desktop/dn280512(v=vs.85).aspx
        private enum DpiAwareness
        {
            None = 0,
            SystemAware = 1,
            PerMonitorAware = 1
        }

        private static bool startMinimized = false;         // When true, the wallpaper manager will not show the MainForm at start
        private static bool hide;                           // When true, the debug console is shown
        public static string loggerID = "WallpaperManager"; // The logger id (name of the the log file)

        public static Logger Logger { get; set; }           // Main logger which will log everything into the logging directory. Generates a new file every day
        public static bool Hide
        {
            get { return hide; }
            set
            {
                hide = value;
                if (hide)
                    HideConsole();
                else
                    UnhideConsole();
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Start logger
            Logger = new Logger(loggerID, Logger.Targets.File);
            Logger.Start();

            InitHideConsole();

            // Allow multiple instances?
            SingleInstanceApplicationLock appLock = new SingleInstanceApplicationLock();
            bool onlyInstance = appLock.TryAcquireExclusiveLock();
            Logger.Info("Is only instance: " + onlyInstance);
            if (!onlyInstance && !Settings.Default.AllowMultipleInstances)
            {
                Logger.Error("Don't start Wallpaper Manager because an instance is alredy running and multiple instances are not allowed!");
                MessageBox.Show("An instance of Wallpaper Manager is already running. If you want multiple instances to be allowed, you can change that in the settings.",
                    "Wallpaper Manager - Multiple instance", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
                return;
            }
            else if (!onlyInstance && Settings.Default.AllowMultipleInstances)
            {
                Logger.Info("An instance is already running. Start anyways because multiple instances are allowed!");
            }
            else
            {
                Logger.Info("Start Wallpaper Manager - Version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            }

            // Disable close button on console so that the user won't accidently close the whole application
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);

            CheckStartup(args);
            InitSettings();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SetProcessDpiAwareness((int)DpiAwareness.PerMonitorAware);

            if (startMinimized)
            {
                new MainForm(startMinimized);
                Application.Run();
            }
            else
            {
                Application.Run(new MainForm(startMinimized));
            }
        }

        /// <summary>
        /// This method will show / hide the console according to the settings.
        /// It also checks if the settings file is corrupted or not, which is a weird bug I've found.
        /// I hope that this error message never has to show up because I don't know how to fix it. It only happend once to me though.
        /// </summary>
        private static void InitHideConsole()
        {
            // BUG: Getting any setting might crash the program because the config file is getting randomly corrupted. I don't know why though
            try
            {
                // Console on start / start minimized
                if (startMinimized || !Settings.Default.ConsoleOnStart)
                    Hide = true;
            }
            catch (Exception e)
            {
                string text = "There was an error initializing Wallpaper Manager! Probably because the user.config file is corrupted.\n" +
                   "Unfortunatly I have no idea why this happens and I can't find anything on the internet. One solution for the future might be not to rename the .exe file "+
                   "(If you have renamed it) I hope I can fix this error in the future.\n\n" +
                   "Right now I can only give you the option to completly reset your settings by deleting the settings file. (Press reset)\n\n" +
                   "I'm very sorry for the inconvinience :(\n\nIf you want to help me fix this bug, you can send a detailed description of your specific problem " +
                   "with the debug information and the exception down below to phil.privat15@gmail.com\n\nExact Exception:\n\n" + e;

                Logger.Fatal(text);
                ConfirmForm form = new ConfirmForm(text, "Reset", "Close", "Ignore");
                form.Text = "Wallpaper Manager - Init Error - PLEASE READ";
                form.ShowDialog();
                Logger.Fatal("User answer: " + form.Result);
                switch (form.Result)
                {
                    case "Reset":
                        Logger.Info("Try delete config file...");
                        //%userprofile%\AppData\Local\%username%
                        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoaming);
                        string path = config.FilePath;
                        try
                        {
                            if (!File.Exists(path))
                                path = path.Replace("Roaming", "Local");
                            File.Delete(path);
                        }
                        catch (Exception exc)
                        {
                            Utils.ShowErrorMessage("Could not delete file. You have to do it yourself (It's easy):\n\n" +
                                @"1. Paste the following in the file explorer ' %userprofile%\AppData\Local\%username% ' and press enter\n\n" +
                                "2. Delete everything with Wallpaper Manager in it\n\n" +
                                "3. Restart\n\nExact Exception:\n\n" + exc);
                        }
                        Logger.Info("Resetted all settings. Restarting...");
                        MessageBox.Show("Resetted all settings. Restart wallpaper manager... I hope it works now :D");
                        System.Diagnostics.Process.Start(Application.ExecutablePath);
                        Environment.Exit(0);
                        break;
                    case "Close":
                        Logger.Info("Close wallpaper manager");
                        Environment.Exit(1);
                        break;
                    case "Ignore":
                        Logger.Info("Just ignore the issue. I bet i crashes lol");
                        break;
                }
            }
        }

        /// <summary>
        /// Apply settings to current run time
        /// </summary>
        private static void InitSettings()
        {
            Logger.Info("Current settings:\n" + Utils.SettingsToString());

            // Events file path
            if (Settings.Default.CurrentPath == "none")
            {
                Settings.Default.CurrentPath = Directory.GetCurrentDirectory() + @"\Events.txt";
                Logger.Info("Initially set event file standard path to: " + Settings.Default.CurrentPath);
            }

            // Log file path
            if (Settings.Default.LogPath == "none")
            {
                Settings.Default.LogPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                Logger.Info("Initially set log file path to: " + Settings.Default.LogPath);
            }

            // Since AllowMultipleInstaces might be set to false because of application restart,
            // we apply the actual preference the user set here after the possbile restart.
            Settings.Default.AllowMultipleInstances = Settings.Default.AllowMultipleInstancesUserPref;

            Settings.Default.Save();
        }

        /// <summary>
        /// Check up startup
        /// </summary>
        /// <param name="args"></param>
        private static void CheckStartup(string[] args)
        {
            if (args.Length != 0 && args[0] == "--minimized"
                || Settings.Default.RestartMinimized)
                startMinimized = true;

            // Check if application is actually in startup
            Settings.Default.InStartup = Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized");

            // Is not in startup but should be
            if (!Settings.Default.InStartup && Settings.Default.ShouldStartup)
            {
                Utils.ShowErrorMessage("According to the application settings the wallpaper manager should be in startup, but it isn't!" + 
                    "\nYou can change this in the settings: Application -> Settings -> Startup");
            }
            // Is in startup but shouldn't be
            else if (Settings.Default.InStartup && !Settings.Default.ShouldStartup)
            {
                Utils.ShowErrorMessage("According to the application settings the wallpaper manager should NOT be in startup, but it is!" +
                    "\nYou can change this in the settings: Application -> Settings -> Startup");
            }
        }

        #region Console Extensions

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        const int SW_HIDE = 0;
        const int SW_UNHIDE = 1;

        private const int MF_BYCOMMAND = 0x00000000;
        public const int SC_CLOSE = 0xF060;

        private static void HideConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            hide = true;
        }

        private static void UnhideConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_UNHIDE);
            hide = false;
        }

        #endregion
    }
}
