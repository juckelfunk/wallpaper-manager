﻿Juckelfunks Wallpaper Manager

Main window controls:
	- The 'Add' button will allow you to add a new wallpaper event like describes below.
	
	- The 'Start' button will start the wallpaper service and will set the wallpapers according to the events in the background
	
	- The 'Stop' button will stop the service. No wallpapers will be set by the wallpaper manager.
	
	- You can always check the service status on the 'status' label.
	  If you started an event manually the service will not be started and though will not be shown as 'started' by the status label

	- You can delete all current events with the 'Delete All' button

	- You can set the next wallpaper manually by clicking the 'Next Wallpaper' Button
	
	- Under the 'File' strip menu you can do actions described by the tool tips. You have to hover over the tool strip menu item.
	
	- Under the 'Application' strip menu you can do actions specific to the application funcionality.
		- The 'Collect garbage' option will remove all unneeded objects from the memory. This may free some of you ram.
		  Note that this option is only implemented for debugging purposes. You should normally see no difference.
		- The 'Toggle Console' option will toggle the debug console and show you information about the things the application is doing.
		  Note that this option is only implemented for debugging purposes.

	- Under the 'About' strip menu you can find this help, an about form with some information and the debug information.

Wallpaper Event Buttons:
	- By clicking on the 'information' button on the left you can modify the event.

	- By clicking on the preview image you can see a fullscreen preview image. You can leave by hitting a key or by clicking with the mouse.
	
	- By clicking the 'i' button you can see information about the wallpaper event in text format.
	
	- By clicking the '!' button you can start an event manually.
	  Note that the status label will not show the service as started because no timers are running in the background.
	
	- By clicking the 'x' button you can remove the wallpaper event.

	- By clicking the '✓' / '✗' button you can enable / disable an event. Disabled events will not be started but rather ignored.
	  You can always start an event by using the '!' button.

	- By clicking the '◨' button you can duplicate the event and add it to the bottom of the list.

Settings:
	- 'Start with login'
		The program will start when the users logs in. This is done by creating a registry key in:
			'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run'
		So if you have any troubles with the autostart (hopefully not), you can fix this manually there.
		The program will start minimized when it is started by startup

	- 'Message on startup'
		When the Wallpaper Manager is started it will show a message.

	- 'Allow multiple instances'
		When checked, you can start as many instances of Wallpaper Manager as you want. Otherwise only one instance
		will be allowed. The Wallpaper Manager will give you an error message and will not start.
		

	- 'Message when minimize'
		When the Wallpaper Manager is minimized to the system tray it will show a message.

	- 'Load previews'
		If this is checked previews of 'wallpaper buttons' and 'wallapper event buttons' will be loaded.
		Because the preview images have to be loaded and then resized, this will take a small time.
		So if you have many wallpapers in an event or many events, you might consider to deactivate the previews.
		This will make loading times faster.

	- 'Show console on startup'
		When the program is started, the console will shown automaticlly. You always can toggle the console window
		with CTRL+T or Application > Toggle console

	- 'Start last event with service start'
		When the service is started, the last event that would have been triggered will be started.
		For example if it is 8:00 and there is an event at 7:00, the 7:00 event is triggered.

	- 'Prefer event over time'
		When the service is started, all events are checked. If an event is true, the event wil be started.
		For example if you have an event that triggers when nightmode is turned on, and you start the service,
		the event will be started when you have nightmode turned on.

	- 'Ask rather than closing'
		Determine if the program will ask you to close or to minimize. When off the program will just quit,
		but then the program will not run in the background. You can always minimize the program to the system tray
		by clicking the minimize button or File > Minimize / CTRL+M


	'Enable logging'
		The console output will be written in a logging file if this is activated. The logging file will be created
		in the logging directory, which you can choose like described below. A new logging file will be created every day.

	- 'Set folder'
		Here you can select the logging folder in which the logging files will be created.

	- You can reset all settings to default by clicking the 'Reset' button and then confirm.

	- To save the settings click the 'OK' button and to exit without saving click the 'Cancel' button.

Running in the Background:
	This program will only work if it is running activly because it is NOT a windows service.
	This means that if you close this program, it will not run and no wallpapers will be set!
	You have to run it in the background.
	
	Fortunatly you can do this easily by clicking the 'minimimze' control
	or by clicking the 'Minimize' option on the File menu strip. The program will then minimize to the system tray
	on the right of the taskbar. You will get a notifycation and the icon will show up. If you want to open the program again,
	simply double click on the icon again. You will be asked what you want to do if you click the 'Close' control.
	
	You can of course completly close the application if you click the 'close' button on the closing warning when clicking the 'Close' control
	or by clicking the 'Close' option in the 'File' menu strip.

	When you click on the notifycation icon you will have see a context menu. You can Start / Stop the service, Set the next wallpaper or terminate
	the wallpaper manager. You can also restart the application which might be useful if you have problems detecting the nigtmode.

Add Wallpaper Event:
	1. Click the 'Add' Button

	2. Import images
		2.1 By clicking the 'Import folder' button you can import all images from a folder
		2.2 By clicling the 'Import images' button you can import specific images

		You can now see all imported images in the wallpaper panel on the left.
		You can also see some basic information like the file name or the image dimensions.
		By clicking on a wallpaper button you can see the preview and make specific settings the the wallpaper.
		By clicking on the preview image you can see a full screen preview which you can close by hitting a button or clicking with the mouse.

	3. Remove image(s)
		3.1 By selecting a wallpaper like described above and then clicking on the 'Remove current' button,
			you can remove the current wallpaper from the wallpaper list.
		3.2 By clicking on the 'Clear panel' button you can remove all wallpapers from the list.

	4. Selecting image style
		4.1 By clicking on one of the "Set all" buttons you can change the image style of all selected wallpapers.
		4.2 By selecting a style from the 'Select style' combo box, you can change the image style of the current wallpaper individually.

	5. Trigger event
		5.1 Set trigger time
			You can specify the trigger time of the event by specifying the hour, minute and second.
			You can do that by selecting the value of one the trigger time combo boxes.
		5.2 Set trigger event
			An event can also be triggered by an event. You can do that by selecting the 'Event' Radio button
			and selecting the event with the 'select event' combobox

	6. Set the note
		You can set a note for the wallpaper event by filling out the 'note' textbox. This might be useful for faster recognition.

	7. Random setting of the wallpapers
		7.1 You can specify if the wallpapers of the wallpaper event should be picked random if the event is runnning.
			The wallpapers will normally no repeat until all wallpapers have been shown.
		7.2 If you want that the wallpapers can repeat themselves while running, select the 'Can repeat random' checkbox.

	8. Set the time interval
		This will specify the time until the next wallpaper of the wallpaper event is set in seconds.
		This obviously will only work if you have selected multiple wallpapers.

	9. Confirm or Cancel
		9.1 By clicking the 'OK' button the wallpaper event will be added or the changes will be saved.
		9.2 By clicking the 'Cancel' button all changes will be lost / the wallpaper event will not be added.

	10. Change the event afterwards
		You can always modifiy the event by clicking the 'information' button on the wallpaper event button.

Trigger Events:
	- Nightmode ON / OFF
		This event will be triggered if the nightmode of windows 10 is turned on / off.
        You need to have the Nightmode Quicksetting button in your ActionCenter to trigger this event correctly.


Dev Notes:
	Note on the nightmode changed event:
		Unfortunaly there is currently no API for the Windows 10 settings. Because of that, I needed to use
		a trick to check whether the nightmode is turned on or off:
			To check if the nightmode is changed I monitor the
				'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\DefaultAccount\
                Cloud\default$windows.data.bluelightreduction.bluelightreductionstate\windows.data.bluelightreduction.bluelightreductionstate'
			registry key. If this key is changed, the nightmode is changed. But because the data is always diffrent,
			we cannot determine if the nightmode is on or off. Only that it has changed.
			
			So what I did is checking the Quick Action button.
				'HKEY_CURRENT_USER\Control Panel\Quick Actions\Control Center\QuickActionsStateCapture'
			
			Now you might ask why I don't mirror only this registry key. Correct! But for some reason the value is not
			changed when the button is clicked?! Well it is changed if you press F5 in regedit, but the wallpaper
			manager does not register the value change. I have no idea why because with the first registry key it works
			just fine. So what I do is that I get the initial nightmode button value and then, when the first registry
			key is changed, I know if the nightmode is turned on or off.

			! THIS SEEMS NOT TO WORK IF YOU CHANGE THE NIGHTMODE AUTOMATICLLY BY TIME !

	Thank you for trying out my wallpaper manager. I developed this tool for myself, but if you can find some use for it is even better.
	Of course it is far from perfect, but it will work at least for me. If you have some ideas or found some bugs please contact me:
		Steam: https://steamcommunity.com/id/juckelfunk/
		Email: phil.privat15@gmail.com
	This program currently is not under any license so do with it what you want.
	I hope this program is at least some use for you. Have a nice day,

~Juckelfunk, 09.09.2019

This help was written for wallpaper manager version 0.2.10

PS: Also sorry for this probably totally useless and horrible help. My english is not the yellow from the egg. ( ͡° ͜ʖ ͡°)


TODO:
	- Sort events by time
	- Fade while setting the wallpaper (maybe with enableing gif as wallpaper (barnacules tutorial) and then generating a gif with fade
	- Multiple monitor support
	- Import events so that they are appended to the current event list
	- Clean up code (e.g. put the service stuff in own class, don't perform just button clicks on e.g. strip menu item clicks,
	    put the code in a method and then call this method from button click and strip menu item)
	- More specific error output when failing to write to the registry
	- Check if time already exists and then dont let the event be generated
	- Update events after log in (events won't start if user is not logged in)
    - Update to UWP
