﻿using System;
using System.Windows.Forms;

/*
 * Title: Information Text Form
 * Author: Juckelfunk
 * Date: 03.06.2018 | 13:16 CET
 * Desc: Show the user information in text form
 */

namespace Wallpaper_Manager
{
    public partial class InfoTextForm : Form
    {
        /// <summary>
        /// Show the user a text in a new form
        /// </summary>
        /// <param name="text"></param>
        public InfoTextForm(string text)
        {
            InitializeComponent();
            
            textBox.Text = text.Replace("\n", Environment.NewLine);
            // There's a bug that selectes all text when set. We prevent this with selecting nothing here
            textBox.Select(0, 0);
        }

        private void OkButton_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}
