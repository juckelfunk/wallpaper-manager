﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

/*
 * Title: Add Wallpaper Event Form
 * Author: Juckelfunk
 * Date: 24.05.2018 | 21:25 CET
 * Desc: Add a wallpaper event to the list
 */

namespace Wallpaper_Manager
{
    public partial class AddWallpaperEventForm : Form
    {
        public WallpaperEvent EventResult { get; set; }                     // The final wallpaperEvent that will be processed by the MainForm / WallpaperEventButton

        private List<Wallpaper> selectedWallpapers = new List<Wallpaper>(); // The wallpapers that are selected
        private Wallpaper currentWallpaper;                                 // Currently selected wallpaper (that one in the preview pictureBox)
        private List<string> pictureExtensions = new List<string>(new string[] { ".jpg", ".jpeg", ".jpe", ".jfif", ".png" } ); // Supported file extensions
        private bool activated = true;                                      // Temp bool to check if an edited event was activated before so we can apply it

        /// <summary>
        /// Add a new wallpaper event to the list
        /// </summary>
        public AddWallpaperEventForm()
        {
            InitializeComponent();
            previewGroupBox.Text = "Preview (" + previewPictureBox.Width + "x" + previewPictureBox.Height + ")";

            // Check the triggerTimeRadioButton so that there is an initial selection of the radio buttons
            triggerTimeRadioButton.Checked = true;
        }

        /// <summary>
        /// Change a already existing wallpaper event
        /// </summary>
        /// <param name="wallpaperEvent">The wallpaper that will be edited</param>
        public AddWallpaperEventForm(WallpaperEvent wallpaperEvent)
        {
            InitializeComponent();

            /* Apply the wallapperEvent values to the UI */

            Program.Logger.Info("Load wallpaper event:'\n" + wallpaperEvent.Note + "'");

            selectedWallpapers = wallpaperEvent.Wallpapers;

            // Trigger time
            triggerHourSelector.Text = wallpaperEvent.Time.Hour.ToString();
            triggerMinuteSelector.Text = wallpaperEvent.Time.Minute.ToString();
            triggerSecondSelector.Text = wallpaperEvent.Time.Second.ToString();

            // Trigger event
            switch (wallpaperEvent.TriggerEvent)
            {
                case WallpaperEvent.TriggerEvents.Nightmode_ON:
                    triggerEventComboBox.Text = "Nightmode ON (Only Windows 10)";
                    break;
                case WallpaperEvent.TriggerEvents.Nightmode_OFF:
                    triggerEventComboBox.Text = "Nightmode OFF (Only Windows 10)";
                    break;
            }
            if (wallpaperEvent.UseTriggerEvent)
                triggerEventRadioButton.Checked = true;
            else
                triggerTimeRadioButton.Checked = true;

            // Misc
            noteTextBox.Text = wallpaperEvent.Note;
            changeIntervalUpDown.Value = wallpaperEvent.InnerChangeTime / 1000;
            randomCheckBox.Checked = wallpaperEvent.Random;
            canRepeatRandomCheckBox.Checked = wallpaperEvent.CanRepeatRandom;
            activated = wallpaperEvent.Activated;
        }

        /// <summary>
        /// Confirm the changes of the wallpaper event and exit with OK result
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            // There has to be at least one image selected
            if (selectedWallpapers.Count == 0)
            {
                Utils.ShowErrorMessage("You have to select at least one image");
                return;
            }

            /* Generate new wallpaper event */

            // Genereate trigger time. Because we only need the mhs set the date to 1.1.1970
            DateTime time = new DateTime(1970, 1, 1,
                Int32.Parse(triggerHourSelector.Text),
                Int32.Parse(triggerMinuteSelector.Text),
                Int32.Parse(triggerSecondSelector.Text));

            // Genereate the trigger event
            WallpaperEvent.TriggerEvents triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_ON;
            switch (triggerEventComboBox.Text)
            {
                case "Nightmode ON (Only Windows 10)":
                    triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_ON;
                    break;
                case "Nightmode OFF (Only Windows 10)":
                    triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_OFF;
                    break;
            }

            // Generate the new wallpaper event and set as the eventResult that will be processed by the MainForm / WallpaperEventButton
            EventResult = new WallpaperEvent(
                noteTextBox.Text,
                time,
                triggerEvent,
                triggerEventRadioButton.Checked,
                selectedWallpapers,
                randomCheckBox.Checked,
                canRepeatRandomCheckBox.Checked,
                Int32.Parse(changeIntervalUpDown.Value.ToString()) * 1000,
                activated);

            Program.Logger.Info("Add wallpaper event form: OK\n" + EventResult);
            
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Close without making changes
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Program.Logger.Info("Add wallapepr event form: CANCEL");
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Remove all wallpapers from the selectedWallpapersPanel
        /// </summary>
        private void ClearSelectedWallpapers()
        {
            if (selectedWallpapersPanel.Controls.Count != 0)
            {
                for (int i = selectedWallpapersPanel.Controls.Count - 1; i >= 0; i--)
                    selectedWallpapersPanel.Controls.RemoveAt(i);
            }
        }

        /// <summary>
        /// Add a wallpaper button to the selectedWallpapersPanel
        /// </summary>
        /// <param name="wallpaper">The wallpaper that will be represented by the wallpaper button</param>
        /// <param name="first">Determine if the wallpaper should be selected automaticlly</param>
        private void AddWallpaperButton(Wallpaper wallpaper, bool first = false)
        {
            WallpaperButton button = new WallpaperButton(wallpaper);
            button.Size = new Size(selectedWallpapersPanel.Size.Width - 20, button.Height);
            button.Location = new Point(0, selectedWallpapersPanel.Controls.Count * (button.Height + 4));
            button.WallpaperButtonClickEvent += new EventHandler(this.WallpaperButtonClick);
            selectedWallpapersPanel.Controls.Add(button);

            // Select the button automatically
            if (first)
                WallpaperButtonClick(button, EventArgs.Empty);

            Program.Logger.Info("Load: " + wallpaper);
        }

        /// <summary>
        /// Add all wallpapers the the selectedWallpapersPanel
        /// </summary>
        private void UpdateWallpapersPanel()
        {
            ClearSelectedWallpapers();

            bool first = true;
            foreach (Wallpaper w in selectedWallpapers)
            {
                AddWallpaperButton(w, first);
                first = false;
            }

            selectedWallpapersGroupBox.Text = "Images - " + selectedWallpapers.Count;
        }

        /// <summary>
        /// Load big preview image and style for this wallpaper
        /// </summary>
        private void WallpaperButtonClick(object sender, EventArgs e)
        {
            WallpaperButton button = sender as WallpaperButton;
            currentWallpaper = button.Wallpaper;

            // Unload previous image from memory
            if (previewPictureBox.Image != null)
                previewPictureBox.Image.Dispose();

            // Set big preview image
            try { previewPictureBox.Image = Image.FromFile(button.Wallpaper.imagePath); }
            catch (Exception exc) { Utils.ShowErrorMessage("Could not load image preview: " + exc); }
            previewGroupBox.Text = "Preview (" + previewPictureBox.Width + "x" + previewPictureBox.Height + ")";

            // Set image style for this wallpaper
            switch (currentWallpaper.ImageStyle)
            {
                case Wallpaper.Style.Stretched:
                    styleComboBox.Text = "Stretched";
                    break;
                case Wallpaper.Style.Centered:
                    styleComboBox.Text = "Centered";
                    break;
                case Wallpaper.Style.Tiled:
                    styleComboBox.Text = "Tiled";
                    break;
                case Wallpaper.Style.Fit:
                    styleComboBox.Text = "Fit";
                    break;
                case Wallpaper.Style.Filled:
                    styleComboBox.Text = "Filled";
                    previewGroupBox.Text = "Preview (" + previewPictureBox.Width + "x" + previewPictureBox.Height + ") - NO CORRECT PREVIEW FOR FILLED STYLE";
                    break;
            }
        }

        /// <summary>
        /// Select single images and import them to the event
        /// </summary>
        private void SelectSingleImagesButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog.ShowDialog();

            // Scroll the panel up to avoid a bug where you have blank space between the old and the new buttons
            if (selectedWallpapersPanel.Controls.Count != 0)
                selectedWallpapersPanel.ScrollControlIntoView(selectedWallpapersPanel.Controls[0]);

            foreach (string file in openFileDialog.FileNames)
            {
                Wallpaper wallpaper = new Wallpaper(file, Wallpaper.Style.Filled);
                selectedWallpapers.Add(wallpaper);
                AddWallpaperButton(wallpaper);
            }
        }

        /// <summary>
        /// Select folder and import all images form it
        /// </summary>
        private void SelectFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            string path = folderBrowserDialog.SelectedPath;

            if (String.IsNullOrEmpty(path))
                return;

            // Scroll the panel up to avoid a bug where you have blank space between the old and the new buttons
            if (selectedWallpapersPanel.Controls.Count != 0)
                selectedWallpapersPanel.ScrollControlIntoView(selectedWallpapersPanel.Controls[0]);

            foreach (string file in Directory.GetFiles(path))
            {
                // File needs to have an supported expansion
                if (pictureExtensions.Contains(Path.GetExtension(file)))
                {
                    Wallpaper wallpaper = new Wallpaper(file, Wallpaper.Style.Filled);
                    selectedWallpapers.Add(wallpaper);
                    AddWallpaperButton(wallpaper);
                }
            }
        }

        /// <summary>
        /// Remove all selected wallpapers
        /// </summary>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            previewPictureBox.Image = null;
            currentWallpaper = null;
            selectedWallpapersGroupBox.Text = "Images";
            selectedWallpapers.Clear();
            ClearSelectedWallpapers();
        }

        /// <summary>
        /// Update the image style of the current wallpaper
        /// </summary>
        private void StyleComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (currentWallpaper != null)
            {
                // Update styles in preview and on current wallpaper
                previewGroupBox.Text = "Preview (" + previewPictureBox.Width + "x" + previewPictureBox.Height + ")";

                // The pictureBox has diffrent stlye names than the wallpapers on windows
                // So choose the style according to the selected image style for the pictureBox
                switch (styleComboBox.Text)
                {
                    case "Stretched":
                        currentWallpaper.ImageStyle = Wallpaper.Style.Stretched;
                        previewPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                        break;
                    case "Centered":
                        currentWallpaper.ImageStyle = Wallpaper.Style.Centered;
                        previewPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                        break;
                    case "Tiled":
                        currentWallpaper.ImageStyle = Wallpaper.Style.Tiled;
                        previewPictureBox.SizeMode = PictureBoxSizeMode.Normal;
                        break;
                    case "Fit":
                        currentWallpaper.ImageStyle = Wallpaper.Style.Fit;
                        previewPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                        break;
                    case "Filled":
                        currentWallpaper.ImageStyle = Wallpaper.Style.Filled;
                        previewPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                        previewGroupBox.Text = "Preview (" + previewPictureBox.Width + "x" + previewPictureBox.Height + ") - NO CORRECT PREVIEW FOR FILLED STYLE";
                        break;
                    default:
                        Utils.ShowErrorMessage("Unknown Image Style choosen!");
                        return;
                }

                // Find current wallpaper in wallpaper list and update the image style
                foreach (Wallpaper w in selectedWallpapers)
                {
                    if (w == currentWallpaper)
                        w.ImageStyle = currentWallpaper.ImageStyle;
                }

                // Find button and change the style label
                foreach (WallpaperButton c in selectedWallpapersPanel.Controls)
                {
                    // Update the text
                    WallpaperButton button = c as WallpaperButton;
                    if (button.Wallpaper == currentWallpaper)
                        button.styleLable.Text = "Style: " + styleComboBox.Text;
                }
            }
            else
            {
                Program.Logger.Error("Cannot change style: No wallpaper selected");
            }
        }

        /// <summary>
        /// Set image styles for all selected wallpapers
        /// </summary>
        private void SetAllStyles(object sender, EventArgs e)
        {
            Button button = sender as Button;
            Wallpaper.Style style = Wallpaper.Style.Centered;

            if (button.Text.Contains("Stretched"))
                style = Wallpaper.Style.Stretched;
            if (button.Text.Contains("Centered"))
                style = Wallpaper.Style.Centered;
            if (button.Text.Contains("Tiled"))
                style = Wallpaper.Style.Tiled;
            if (button.Text.Contains("Fit"))
                style = Wallpaper.Style.Fit;
            if (button.Text.Contains("Filled"))
                style = Wallpaper.Style.Filled;
            
            // Update image style comboBox and all selected wallpapers
            styleComboBox.Text = button.Text.Substring(4);
            foreach (Wallpaper w in selectedWallpapers)
                w.ImageStyle = style;

            // Update wallpaper buttons
            foreach (WallpaperButton c in selectedWallpapersPanel.Controls)
            {
                WallpaperButton wallpaperButton = c as WallpaperButton;
                wallpaperButton.styleLable.Text = "Style: " + button.Text.Substring(4);
            }
        }

        /// <summary>
        /// Remove current wallpaper from the list
        /// </summary>
        private void RemoveWallpaper_Click(object sender, EventArgs e)
        {
            // Find current wallpaper in wallpaper list
            int i = 0;
            foreach (Wallpaper w in selectedWallpapers)
            {
                if (w == currentWallpaper)
                    break;
                i++;
            }

            // Remove current wallpaper and regenerate the panel
            // TODO: Find a faster solution
            selectedWallpapers.RemoveAt(i);
            UpdateWallpapersPanel();
        }

        /// <summary>
        /// Dispose the big preview image
        /// </summary>
        private void AddWallpaperEventForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (previewPictureBox.Image != null)
                previewPictureBox.Image.Dispose();

            // Dispose Wallpaperbutton previews
            foreach (Control c in selectedWallpapersPanel.Controls)
            {
                if (c.GetType() == typeof(WallpaperButton))
                {
                    WallpaperButton wb = c as WallpaperButton;
                    wb.DisposePreview();
                }
            }
        }

        /// <summary>
        /// Set the title to loading and add the wallpapers
        /// </summary>
        private void AddWallpaperEventForm_Shown(object sender, EventArgs e)
        {
            string currentText = Text;
            Text += " - Loading";
            UpdateWallpapersPanel();
            Text = currentText;
        }

        /// <summary>
        /// Set Hand cursor to let the user know that he can click something
        /// </summary>
        private void Hover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        /// <summary>
        /// Set the cursor back to normal
        /// </summary>
        private void UnHover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Arrow;
        }

        /// <summary>
        /// Show a fullscreen preview of the selected wallpaper according to the image style
        /// </summary>
        private void PreviewPictureBox_Click(object sender, EventArgs e)
        {
            if (currentWallpaper == null)
                return;
            ShowImageForm form = new ShowImageForm(currentWallpaper);
            form.ShowDialog();
        }

        /// <summary>
        /// Enable / Disable the user controls according to the selected trigger RadioButton
        /// </summary>
        private void SelectTrigger(object sender, EventArgs e)
        {
            bool enable = triggerTimeRadioButton.Checked;
            triggerHourSelector.Enabled = enable;
            triggerMinuteSelector.Enabled = enable;
            triggerSecondSelector.Enabled = enable;
            triggerEventComboBox.Enabled = !enable;
        }

        /// <summary>
        /// Show a short readme informing about the trigger events
        /// </summary>
        private void ReadMeLabel_Click(object sender, EventArgs e)
        {
            string text = "You will have to add the Nightmode Quicksetting button to your ActionCenter.\n" +
                "Otherwise the event will not trigger!\n\n" +
                "For more information about this issue read the help file.\n" +
                "MainForm->About->Help";
            MessageBox.Show(text, "IMPORTANT INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Confirm with the enter key and cancel with the esc key
        /// </summary>
        private void AddWallpaperEventForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult res = MessageBox.Show("Save and exit to main window?", "Save", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Cancel)
                    return;
                okButton.PerformClick();
            }
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult res = MessageBox.Show("Do NOT save and exit to main window?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Cancel)
                    return;
                cancelButton.PerformClick();
            }
        }
    }
}
