﻿namespace Wallpaper_Manager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.eventPanel = new System.Windows.Forms.Panel();
            this.stopServiceButton = new System.Windows.Forms.Button();
            this.startServiceButton = new System.Windows.Forms.Button();
            this.addEventButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.serviceStatusLabel = new System.Windows.Forms.Label();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.notifyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nextWallpaperMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startStopServiceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectGarbageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleConsoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadEventsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToSystemTrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectGarbageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleConsoleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.startServiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abouzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugInformationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.deleteAllButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.notifyContextMenuStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // eventPanel
            // 
            this.eventPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventPanel.AutoScroll = true;
            this.eventPanel.Location = new System.Drawing.Point(18, 42);
            this.eventPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventPanel.Name = "eventPanel";
            this.eventPanel.Size = new System.Drawing.Size(688, 691);
            this.eventPanel.TabIndex = 0;
            // 
            // stopServiceButton
            // 
            this.stopServiceButton.Location = new System.Drawing.Point(18, 829);
            this.stopServiceButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.stopServiceButton.Name = "stopServiceButton";
            this.stopServiceButton.Size = new System.Drawing.Size(112, 35);
            this.stopServiceButton.TabIndex = 3;
            this.stopServiceButton.Text = "Stop";
            this.toolTip.SetToolTip(this.stopServiceButton, "Stop Wallpape Service");
            this.stopServiceButton.UseVisualStyleBackColor = true;
            this.stopServiceButton.Click += new System.EventHandler(this.StopServiceButton_Click);
            // 
            // startServiceButton
            // 
            this.startServiceButton.Location = new System.Drawing.Point(18, 785);
            this.startServiceButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.startServiceButton.Name = "startServiceButton";
            this.startServiceButton.Size = new System.Drawing.Size(112, 35);
            this.startServiceButton.TabIndex = 2;
            this.startServiceButton.Text = "Start";
            this.toolTip.SetToolTip(this.startServiceButton, "Start Wallpaper service");
            this.startServiceButton.UseVisualStyleBackColor = true;
            this.startServiceButton.Click += new System.EventHandler(this.StartServiceButton_Click);
            // 
            // addEventButton
            // 
            this.addEventButton.Location = new System.Drawing.Point(596, 742);
            this.addEventButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addEventButton.Name = "addEventButton";
            this.addEventButton.Size = new System.Drawing.Size(112, 35);
            this.addEventButton.TabIndex = 1;
            this.addEventButton.Text = "Add";
            this.toolTip.SetToolTip(this.addEventButton, "Add event to current events");
            this.addEventButton.UseVisualStyleBackColor = true;
            this.addEventButton.Click += new System.EventHandler(this.AddEventButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(596, 831);
            this.okButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(112, 35);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.toolTip.SetToolTip(this.okButton, "Apply changes and minimize to system tray");
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(474, 829);
            this.applyButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(112, 35);
            this.applyButton.TabIndex = 5;
            this.applyButton.Text = "Apply";
            this.toolTip.SetToolTip(this.applyButton, "Apply changes and save events to file");
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // serviceStatusLabel
            // 
            this.serviceStatusLabel.AutoSize = true;
            this.serviceStatusLabel.Location = new System.Drawing.Point(141, 845);
            this.serviceStatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.serviceStatusLabel.Name = "serviceStatusLabel";
            this.serviceStatusLabel.Size = new System.Drawing.Size(130, 20);
            this.serviceStatusLabel.TabIndex = 6;
            this.serviceStatusLabel.Text = "Service: Stopped";
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.notifyContextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Wallpaper Manager";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_DoubleMouseClick);
            // 
            // notifyContextMenuStrip
            // 
            this.notifyContextMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.notifyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nextWallpaperMenuItem,
            this.startStopServiceMenuItem,
            this.openStripMenuItem,
            this.restartToolStripMenuItem1,
            this.quitToolStripMenuItem});
            this.notifyContextMenuStrip.Name = "notifyContextMenuStrip";
            this.notifyContextMenuStrip.Size = new System.Drawing.Size(277, 164);
            // 
            // nextWallpaperMenuItem
            // 
            this.nextWallpaperMenuItem.Name = "nextWallpaperMenuItem";
            this.nextWallpaperMenuItem.Size = new System.Drawing.Size(276, 32);
            this.nextWallpaperMenuItem.Text = "Next Wallpaper";
            this.nextWallpaperMenuItem.Click += new System.EventHandler(this.NextWallpaperMenuItem_Click);
            // 
            // startStopServiceMenuItem
            // 
            this.startStopServiceMenuItem.Name = "startStopServiceMenuItem";
            this.startStopServiceMenuItem.Size = new System.Drawing.Size(276, 32);
            this.startStopServiceMenuItem.Text = "Start / Stop Service";
            this.startStopServiceMenuItem.Click += new System.EventHandler(this.StartStopServiceMenuItem_Click);
            // 
            // openStripMenuItem
            // 
            this.openStripMenuItem.Name = "openStripMenuItem";
            this.openStripMenuItem.Size = new System.Drawing.Size(276, 32);
            this.openStripMenuItem.Text = "Open Wallaper Manager";
            this.openStripMenuItem.Click += new System.EventHandler(this.OpenStripMenuItem_Click);
            // 
            // restartToolStripMenuItem1
            // 
            this.restartToolStripMenuItem1.Name = "restartToolStripMenuItem1";
            this.restartToolStripMenuItem1.Size = new System.Drawing.Size(276, 32);
            this.restartToolStripMenuItem1.Text = "Restart";
            this.restartToolStripMenuItem1.Click += new System.EventHandler(this.RestartToolStripMenuItem1_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(276, 32);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // collectGarbageToolStripMenuItem
            // 
            this.collectGarbageToolStripMenuItem.Name = "collectGarbageToolStripMenuItem";
            this.collectGarbageToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.collectGarbageToolStripMenuItem.Text = "Collect Garbage";
            this.collectGarbageToolStripMenuItem.Click += new System.EventHandler(this.CollectGarbageToolStripMenuItem_Click);
            // 
            // toggleConsoleToolStripMenuItem
            // 
            this.toggleConsoleToolStripMenuItem.Name = "toggleConsoleToolStripMenuItem";
            this.toggleConsoleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.toggleConsoleToolStripMenuItem.Text = "Toggle Console";
            this.toggleConsoleToolStripMenuItem.Click += new System.EventHandler(this.ToggleConsoleToolStripMenuItem_Click);
            // 
            // startServiceToolStripMenuItem
            // 
            this.startServiceToolStripMenuItem.Name = "startServiceToolStripMenuItem";
            this.startServiceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.startServiceToolStripMenuItem.Text = "Start Service";
            this.startServiceToolStripMenuItem.Click += new System.EventHandler(this.StartServiceToolStripMenuItem_Click);
            // 
            // stopServiceToolStripMenuItem
            // 
            this.stopServiceToolStripMenuItem.Name = "stopServiceToolStripMenuItem";
            this.stopServiceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.stopServiceToolStripMenuItem.Text = "Stop Service";
            this.stopServiceToolStripMenuItem.Click += new System.EventHandler(this.StopServiceToolStripMenuItem_Click);
            // 
            // debugInformationToolStripMenuItem
            // 
            this.debugInformationToolStripMenuItem.Name = "debugInformationToolStripMenuItem";
            this.debugInformationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.debugInformationToolStripMenuItem.Text = "Debug Information";
            this.debugInformationToolStripMenuItem.Click += new System.EventHandler(this.DebugInformationToolStripMenuItem_Click);
            // 
            // minimizeToolStripMenuItem
            // 
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            this.minimizeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.minimizeToolStripMenuItem.Text = "Minimize";
            this.minimizeToolStripMenuItem.Click += new System.EventHandler(this.MinimizeToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.applicationToolStripMenuItem,
            this.abouzToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1086, 54);
            this.menuStrip.TabIndex = 7;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveEventsToolStripMenuItem,
            this.loadEventsToolStripMenuItem,
            this.importEventsToolStripMenuItem,
            this.exportEventsToolStripMenuItem,
            this.reloadEventsToolStripMenuItem,
            this.minimizeToSystemTrayToolStripMenuItem,
            this.restartToolStripMenuItem,
            this.closeToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "Alt+F";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(54, 48);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveEventsToolStripMenuItem
            // 
            this.saveEventsToolStripMenuItem.Name = "saveEventsToolStripMenuItem";
            this.saveEventsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl + S";
            this.saveEventsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveEventsToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.saveEventsToolStripMenuItem.Text = "Save Events";
            this.saveEventsToolStripMenuItem.ToolTipText = "Save changes";
            this.saveEventsToolStripMenuItem.Click += new System.EventHandler(this.ApplyToolStripMenuItem_Click);
            // 
            // loadEventsToolStripMenuItem
            // 
            this.loadEventsToolStripMenuItem.Name = "loadEventsToolStripMenuItem";
            this.loadEventsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+O";
            this.loadEventsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadEventsToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.loadEventsToolStripMenuItem.Text = "Load Events";
            this.loadEventsToolStripMenuItem.ToolTipText = "Load Events from file";
            this.loadEventsToolStripMenuItem.Click += new System.EventHandler(this.LoadEventsToolStripMenuItem_Click);
            // 
            // importEventsToolStripMenuItem
            // 
            this.importEventsToolStripMenuItem.Name = "importEventsToolStripMenuItem";
            this.importEventsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+I";
            this.importEventsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.I)));
            this.importEventsToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.importEventsToolStripMenuItem.Text = "Import Events";
            this.importEventsToolStripMenuItem.ToolTipText = "Add events from file to current events";
            this.importEventsToolStripMenuItem.Click += new System.EventHandler(this.ImportEventsToolStripMenuItem_Click);
            // 
            // exportEventsToolStripMenuItem
            // 
            this.exportEventsToolStripMenuItem.Name = "exportEventsToolStripMenuItem";
            this.exportEventsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E";
            this.exportEventsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exportEventsToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.exportEventsToolStripMenuItem.Text = "Export Events";
            this.exportEventsToolStripMenuItem.ToolTipText = "Export current events to file";
            this.exportEventsToolStripMenuItem.Click += new System.EventHandler(this.ExportEventsToolStripMenuItem_Click);
            // 
            // reloadEventsToolStripMenuItem
            // 
            this.reloadEventsToolStripMenuItem.Name = "reloadEventsToolStripMenuItem";
            this.reloadEventsToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+R";
            this.reloadEventsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.reloadEventsToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.reloadEventsToolStripMenuItem.Text = "Reload Events";
            this.reloadEventsToolStripMenuItem.ToolTipText = "Reload current events from file";
            this.reloadEventsToolStripMenuItem.Click += new System.EventHandler(this.ReloadToolStripMenuItem_Click);
            // 
            // minimizeToSystemTrayToolStripMenuItem
            // 
            this.minimizeToSystemTrayToolStripMenuItem.Name = "minimizeToSystemTrayToolStripMenuItem";
            this.minimizeToSystemTrayToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+M";
            this.minimizeToSystemTrayToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.minimizeToSystemTrayToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.minimizeToSystemTrayToolStripMenuItem.Text = "Minimize";
            this.minimizeToSystemTrayToolStripMenuItem.ToolTipText = "Minimize to system tray (Will run in background)";
            this.minimizeToSystemTrayToolStripMenuItem.Click += new System.EventHandler(this.MinimizeToolStripMenuItem_Click);
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+R";
            this.restartToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(329, 34);
            this.restartToolStripMenuItem.Text = "Restart";
            this.restartToolStripMenuItem.Click += new System.EventHandler(this.RestartToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem1
            // 
            this.closeToolStripMenuItem1.Name = "closeToolStripMenuItem1";
            this.closeToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeToolStripMenuItem1.Size = new System.Drawing.Size(329, 34);
            this.closeToolStripMenuItem1.Text = "Close";
            this.closeToolStripMenuItem1.ToolTipText = "Close Application (Will not run in background and won\'t set wallpapers)";
            this.closeToolStripMenuItem1.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // applicationToolStripMenuItem
            // 
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collectGarbageToolStripMenuItem1,
            this.toggleConsoleToolStripMenuItem1,
            this.startServiceToolStripMenuItem1,
            this.stopServiceToolStripMenuItem1,
            this.settingsToolStripMenuItem1});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            this.applicationToolStripMenuItem.ShortcutKeyDisplayString = "Alt+A";
            this.applicationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.applicationToolStripMenuItem.Size = new System.Drawing.Size(118, 48);
            this.applicationToolStripMenuItem.Text = "Application";
            // 
            // collectGarbageToolStripMenuItem1
            // 
            this.collectGarbageToolStripMenuItem1.Name = "collectGarbageToolStripMenuItem1";
            this.collectGarbageToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+C";
            this.collectGarbageToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.collectGarbageToolStripMenuItem1.Size = new System.Drawing.Size(354, 34);
            this.collectGarbageToolStripMenuItem1.Text = "Collect Garbage";
            this.collectGarbageToolStripMenuItem1.ToolTipText = "Free unneeded data from memory\r\nOnly for debug purposes";
            this.collectGarbageToolStripMenuItem1.Click += new System.EventHandler(this.CollectGarbageToolStripMenuItem_Click);
            // 
            // toggleConsoleToolStripMenuItem1
            // 
            this.toggleConsoleToolStripMenuItem1.Name = "toggleConsoleToolStripMenuItem1";
            this.toggleConsoleToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+T";
            this.toggleConsoleToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.toggleConsoleToolStripMenuItem1.Size = new System.Drawing.Size(354, 34);
            this.toggleConsoleToolStripMenuItem1.Text = "Toggle Console";
            this.toggleConsoleToolStripMenuItem1.ToolTipText = "Show Debug Console\r\nOnly for debug purposes";
            this.toggleConsoleToolStripMenuItem1.Click += new System.EventHandler(this.ToggleConsoleToolStripMenuItem_Click);
            // 
            // startServiceToolStripMenuItem1
            // 
            this.startServiceToolStripMenuItem1.Name = "startServiceToolStripMenuItem1";
            this.startServiceToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.startServiceToolStripMenuItem1.Size = new System.Drawing.Size(354, 34);
            this.startServiceToolStripMenuItem1.Text = "Start Service";
            this.startServiceToolStripMenuItem1.ToolTipText = "Start Wallpaper Service";
            this.startServiceToolStripMenuItem1.Click += new System.EventHandler(this.StartServiceToolStripMenuItem_Click);
            // 
            // stopServiceToolStripMenuItem1
            // 
            this.stopServiceToolStripMenuItem1.Name = "stopServiceToolStripMenuItem1";
            this.stopServiceToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.stopServiceToolStripMenuItem1.Size = new System.Drawing.Size(354, 34);
            this.stopServiceToolStripMenuItem1.Text = "Stop Service";
            this.stopServiceToolStripMenuItem1.ToolTipText = "Stop Wallpaper Service";
            this.stopServiceToolStripMenuItem1.Click += new System.EventHandler(this.StopServiceToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+I";
            this.settingsToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(354, 34);
            this.settingsToolStripMenuItem1.Text = "Settings";
            this.settingsToolStripMenuItem1.Click += new System.EventHandler(this.SettingsToolStripMenuItem1_Click);
            // 
            // abouzToolStripMenuItem
            // 
            this.abouzToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.debugInformationToolStripMenuItem1,
            this.helpToolStripMenuItem});
            this.abouzToolStripMenuItem.Name = "abouzToolStripMenuItem";
            this.abouzToolStripMenuItem.ShortcutKeyDisplayString = "Alt+B";
            this.abouzToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.abouzToolStripMenuItem.Size = new System.Drawing.Size(78, 48);
            this.abouzToolStripMenuItem.Text = "About";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(331, 34);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.ToolTipText = "About this program";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // debugInformationToolStripMenuItem1
            // 
            this.debugInformationToolStripMenuItem1.Name = "debugInformationToolStripMenuItem1";
            this.debugInformationToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+D";
            this.debugInformationToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.debugInformationToolStripMenuItem1.Size = new System.Drawing.Size(331, 34);
            this.debugInformationToolStripMenuItem1.Text = "Debug Information";
            this.debugInformationToolStripMenuItem1.ToolTipText = "Show debug information\r\nOnly for debug purposes";
            this.debugInformationToolStripMenuItem1.Click += new System.EventHandler(this.DebugInformationToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+H";
            this.helpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(331, 34);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // deleteAllButton
            // 
            this.deleteAllButton.Location = new System.Drawing.Point(474, 742);
            this.deleteAllButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deleteAllButton.Name = "deleteAllButton";
            this.deleteAllButton.Size = new System.Drawing.Size(112, 35);
            this.deleteAllButton.TabIndex = 8;
            this.deleteAllButton.Text = "Delete All";
            this.toolTip.SetToolTip(this.deleteAllButton, "Add event to current events");
            this.deleteAllButton.UseVisualStyleBackColor = true;
            this.deleteAllButton.Click += new System.EventHandler(this.DeleteAllButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(352, 831);
            this.nextButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(112, 35);
            this.nextButton.TabIndex = 4;
            this.nextButton.Text = "Next";
            this.toolTip.SetToolTip(this.nextButton, "Set next wallpaper in list");
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(724, 877);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.deleteAllButton);
            this.Controls.Add(this.serviceStatusLabel);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.addEventButton);
            this.Controls.Add(this.startServiceButton);
            this.Controls.Add(this.stopServiceButton);
            this.Controls.Add(this.eventPanel);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Juckelfunks Wallpaper Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.notifyContextMenuStrip.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel eventPanel;
        private System.Windows.Forms.Button stopServiceButton;
        private System.Windows.Forms.Button startServiceButton;
        private System.Windows.Forms.Button addEventButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Label serviceStatusLabel;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectGarbageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleConsoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadEventsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToSystemTrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectGarbageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toggleConsoleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem startServiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stopServiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abouzToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugInformationToolStripMenuItem1;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.Button deleteAllButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.ContextMenuStrip notifyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem nextWallpaperMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startStopServiceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem1;
    }
}