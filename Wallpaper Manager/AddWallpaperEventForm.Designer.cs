﻿namespace Wallpaper_Manager
{
    partial class AddWallpaperEventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddWallpaperEventForm));
            this.triggerTimeLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.triggerHourSelector = new System.Windows.Forms.ComboBox();
            this.triggerMinuteSelector = new System.Windows.Forms.ComboBox();
            this.triggerSecondSelector = new System.Windows.Forms.ComboBox();
            this.selectSingleImageButton = new System.Windows.Forms.Button();
            this.selectFolderButton = new System.Windows.Forms.Button();
            this.selectedWallpapersPanel = new System.Windows.Forms.Panel();
            this.selectedWallpapersGroupBox = new System.Windows.Forms.GroupBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.noteLabel = new System.Windows.Forms.Label();
            this.noteTextBox = new System.Windows.Forms.TextBox();
            this.previewGroupBox = new System.Windows.Forms.GroupBox();
            this.previewPictureBox = new System.Windows.Forms.PictureBox();
            this.styleComboBox = new System.Windows.Forms.ComboBox();
            this.styleLabel = new System.Windows.Forms.Label();
            this.allStretchedButton = new System.Windows.Forms.Button();
            this.allCenteredButton = new System.Windows.Forms.Button();
            this.allTiledButton = new System.Windows.Forms.Button();
            this.changeIntervalUpDown = new System.Windows.Forms.NumericUpDown();
            this.changeIntervalLabel = new System.Windows.Forms.Label();
            this.randomCheckBox = new System.Windows.Forms.CheckBox();
            this.canRepeatRandomCheckBox = new System.Windows.Forms.CheckBox();
            this.removeWallpaper = new System.Windows.Forms.Button();
            this.allFitButton = new System.Windows.Forms.Button();
            this.allFilledButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.triggerTimeRadioButton = new System.Windows.Forms.RadioButton();
            this.triggerEventRadioButton = new System.Windows.Forms.RadioButton();
            this.triggerEventComboBox = new System.Windows.Forms.ComboBox();
            this.readMeLabel = new System.Windows.Forms.Label();
            this.selectedWallpapersGroupBox.SuspendLayout();
            this.previewGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeIntervalUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // triggerTimeLabel
            // 
            this.triggerTimeLabel.AutoSize = true;
            this.triggerTimeLabel.Location = new System.Drawing.Point(99, 18);
            this.triggerTimeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.triggerTimeLabel.Name = "triggerTimeLabel";
            this.triggerTimeLabel.Size = new System.Drawing.Size(96, 20);
            this.triggerTimeLabel.TabIndex = 0;
            this.triggerTimeLabel.Text = "Trigger Time";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point(1136, 652);
            this.okButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(112, 35);
            this.okButton.TabIndex = 19;
            this.okButton.Text = "OK";
            this.toolTip.SetToolTip(this.okButton, "Apply changes / Add event");
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(1014, 652);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 20;
            this.cancelButton.Text = "Cancel";
            this.toolTip.SetToolTip(this.cancelButton, "Do not apply changes / add event");
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // triggerHourSelector
            // 
            this.triggerHourSelector.ForeColor = System.Drawing.SystemColors.WindowText;
            this.triggerHourSelector.FormattingEnabled = true;
            this.triggerHourSelector.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.triggerHourSelector.Location = new System.Drawing.Point(104, 43);
            this.triggerHourSelector.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerHourSelector.MaxDropDownItems = 12;
            this.triggerHourSelector.Name = "triggerHourSelector";
            this.triggerHourSelector.Size = new System.Drawing.Size(56, 28);
            this.triggerHourSelector.TabIndex = 3;
            this.triggerHourSelector.Text = "0";
            this.toolTip.SetToolTip(this.triggerHourSelector, "Hour trigger time");
            // 
            // triggerMinuteSelector
            // 
            this.triggerMinuteSelector.DropDownWidth = 100;
            this.triggerMinuteSelector.FormattingEnabled = true;
            this.triggerMinuteSelector.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.triggerMinuteSelector.Location = new System.Drawing.Point(171, 43);
            this.triggerMinuteSelector.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerMinuteSelector.MaxDropDownItems = 20;
            this.triggerMinuteSelector.Name = "triggerMinuteSelector";
            this.triggerMinuteSelector.Size = new System.Drawing.Size(56, 28);
            this.triggerMinuteSelector.TabIndex = 4;
            this.triggerMinuteSelector.Text = "0";
            this.toolTip.SetToolTip(this.triggerMinuteSelector, "Minute trigger time");
            // 
            // triggerSecondSelector
            // 
            this.triggerSecondSelector.DropDownWidth = 100;
            this.triggerSecondSelector.FormattingEnabled = true;
            this.triggerSecondSelector.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.triggerSecondSelector.Location = new System.Drawing.Point(238, 43);
            this.triggerSecondSelector.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerSecondSelector.MaxDropDownItems = 20;
            this.triggerSecondSelector.Name = "triggerSecondSelector";
            this.triggerSecondSelector.Size = new System.Drawing.Size(56, 28);
            this.triggerSecondSelector.TabIndex = 5;
            this.triggerSecondSelector.Text = "0";
            this.toolTip.SetToolTip(this.triggerSecondSelector, "Second trigger time");
            // 
            // selectSingleImageButton
            // 
            this.selectSingleImageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.selectSingleImageButton.Location = new System.Drawing.Point(360, 652);
            this.selectSingleImageButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectSingleImageButton.Name = "selectSingleImageButton";
            this.selectSingleImageButton.Size = new System.Drawing.Size(136, 35);
            this.selectSingleImageButton.TabIndex = 7;
            this.selectSingleImageButton.Text = "Select Image";
            this.toolTip.SetToolTip(this.selectSingleImageButton, "Import selected images");
            this.selectSingleImageButton.UseVisualStyleBackColor = true;
            this.selectSingleImageButton.Click += new System.EventHandler(this.SelectSingleImagesButton_Click);
            // 
            // selectFolderButton
            // 
            this.selectFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.selectFolderButton.Location = new System.Drawing.Point(214, 652);
            this.selectFolderButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectFolderButton.Name = "selectFolderButton";
            this.selectFolderButton.Size = new System.Drawing.Size(136, 35);
            this.selectFolderButton.TabIndex = 8;
            this.selectFolderButton.Text = "Select Folder";
            this.toolTip.SetToolTip(this.selectFolderButton, "Import whole folder");
            this.selectFolderButton.UseVisualStyleBackColor = true;
            this.selectFolderButton.Click += new System.EventHandler(this.SelectFolderButton_Click);
            // 
            // selectedWallpapersPanel
            // 
            this.selectedWallpapersPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectedWallpapersPanel.AutoScroll = true;
            this.selectedWallpapersPanel.Location = new System.Drawing.Point(9, 29);
            this.selectedWallpapersPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectedWallpapersPanel.Name = "selectedWallpapersPanel";
            this.selectedWallpapersPanel.Size = new System.Drawing.Size(460, 465);
            this.selectedWallpapersPanel.TabIndex = 0;
            // 
            // selectedWallpapersGroupBox
            // 
            this.selectedWallpapersGroupBox.Controls.Add(this.selectedWallpapersPanel);
            this.selectedWallpapersGroupBox.Location = new System.Drawing.Point(18, 140);
            this.selectedWallpapersGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectedWallpapersGroupBox.Name = "selectedWallpapersGroupBox";
            this.selectedWallpapersGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectedWallpapersGroupBox.Size = new System.Drawing.Size(478, 503);
            this.selectedWallpapersGroupBox.TabIndex = 0;
            this.selectedWallpapersGroupBox.TabStop = false;
            this.selectedWallpapersGroupBox.Text = "Images";
            // 
            // clearButton
            // 
            this.clearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearButton.Location = new System.Drawing.Point(18, 652);
            this.clearButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(112, 35);
            this.clearButton.TabIndex = 9;
            this.clearButton.Text = "Clear";
            this.toolTip.SetToolTip(this.clearButton, "Remove all events from list");
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // noteLabel
            // 
            this.noteLabel.AutoSize = true;
            this.noteLabel.Location = new System.Drawing.Point(320, 18);
            this.noteLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.noteLabel.Name = "noteLabel";
            this.noteLabel.Size = new System.Drawing.Size(43, 20);
            this.noteLabel.TabIndex = 0;
            this.noteLabel.Text = "Note";
            // 
            // noteTextBox
            // 
            this.noteTextBox.Location = new System.Drawing.Point(324, 43);
            this.noteTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.noteTextBox.Name = "noteTextBox";
            this.noteTextBox.Size = new System.Drawing.Size(148, 26);
            this.noteTextBox.TabIndex = 6;
            this.noteTextBox.Text = "New Event";
            this.toolTip.SetToolTip(this.noteTextBox, "Note for the event");
            // 
            // previewGroupBox
            // 
            this.previewGroupBox.Controls.Add(this.previewPictureBox);
            this.previewGroupBox.Location = new System.Drawing.Point(507, 140);
            this.previewGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.previewGroupBox.Name = "previewGroupBox";
            this.previewGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.previewGroupBox.Size = new System.Drawing.Size(741, 455);
            this.previewGroupBox.TabIndex = 0;
            this.previewGroupBox.TabStop = false;
            this.previewGroupBox.Text = "Preview";
            // 
            // previewPictureBox
            // 
            this.previewPictureBox.Location = new System.Drawing.Point(10, 31);
            this.previewPictureBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.previewPictureBox.Name = "previewPictureBox";
            this.previewPictureBox.Size = new System.Drawing.Size(720, 415);
            this.previewPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewPictureBox.TabIndex = 0;
            this.previewPictureBox.TabStop = false;
            this.previewPictureBox.Click += new System.EventHandler(this.PreviewPictureBox_Click);
            this.previewPictureBox.MouseLeave += new System.EventHandler(this.UnHover);
            this.previewPictureBox.MouseHover += new System.EventHandler(this.Hover);
            // 
            // styleComboBox
            // 
            this.styleComboBox.FormattingEnabled = true;
            this.styleComboBox.Items.AddRange(new object[] {
            "Stretched",
            "Centered",
            "Tiled",
            "Fit",
            "Filled"});
            this.styleComboBox.Location = new System.Drawing.Point(506, 43);
            this.styleComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.styleComboBox.Name = "styleComboBox";
            this.styleComboBox.Size = new System.Drawing.Size(180, 28);
            this.styleComboBox.TabIndex = 10;
            this.styleComboBox.Text = "Filled";
            this.toolTip.SetToolTip(this.styleComboBox, "Style of the wallpaper when set\r\nNO PREVIEW FOR FILLED");
            this.styleComboBox.SelectedIndexChanged += new System.EventHandler(this.StyleComboBox_SelectedIndexChanged);
            // 
            // styleLabel
            // 
            this.styleLabel.AutoSize = true;
            this.styleLabel.Location = new System.Drawing.Point(501, 18);
            this.styleLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.styleLabel.Name = "styleLabel";
            this.styleLabel.Size = new System.Drawing.Size(44, 20);
            this.styleLabel.TabIndex = 0;
            this.styleLabel.Text = "Style";
            // 
            // allStretchedButton
            // 
            this.allStretchedButton.Location = new System.Drawing.Point(939, 18);
            this.allStretchedButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allStretchedButton.Name = "allStretchedButton";
            this.allStretchedButton.Size = new System.Drawing.Size(112, 35);
            this.allStretchedButton.TabIndex = 15;
            this.allStretchedButton.Text = "All Stretched";
            this.toolTip.SetToolTip(this.allStretchedButton, "Set all wallpapers to streched style");
            this.allStretchedButton.UseVisualStyleBackColor = true;
            this.allStretchedButton.Click += new System.EventHandler(this.SetAllStyles);
            // 
            // allCenteredButton
            // 
            this.allCenteredButton.Location = new System.Drawing.Point(818, 18);
            this.allCenteredButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allCenteredButton.Name = "allCenteredButton";
            this.allCenteredButton.Size = new System.Drawing.Size(112, 35);
            this.allCenteredButton.TabIndex = 13;
            this.allCenteredButton.Text = "All Centered";
            this.toolTip.SetToolTip(this.allCenteredButton, "Set all wallpapers to centered style");
            this.allCenteredButton.UseVisualStyleBackColor = true;
            this.allCenteredButton.Click += new System.EventHandler(this.SetAllStyles);
            // 
            // allTiledButton
            // 
            this.allTiledButton.Location = new System.Drawing.Point(818, 63);
            this.allTiledButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allTiledButton.Name = "allTiledButton";
            this.allTiledButton.Size = new System.Drawing.Size(112, 35);
            this.allTiledButton.TabIndex = 14;
            this.allTiledButton.Text = "All Tiled";
            this.toolTip.SetToolTip(this.allTiledButton, "Set all wallpapers to tiled style");
            this.allTiledButton.UseVisualStyleBackColor = true;
            this.allTiledButton.Click += new System.EventHandler(this.SetAllStyles);
            // 
            // changeIntervalUpDown
            // 
            this.changeIntervalUpDown.Location = new System.Drawing.Point(754, 631);
            this.changeIntervalUpDown.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.changeIntervalUpDown.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.changeIntervalUpDown.Name = "changeIntervalUpDown";
            this.changeIntervalUpDown.Size = new System.Drawing.Size(180, 26);
            this.changeIntervalUpDown.TabIndex = 18;
            this.toolTip.SetToolTip(this.changeIntervalUpDown, "Time until the wallpapers are changed in this event");
            this.changeIntervalUpDown.Value = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            // 
            // changeIntervalLabel
            // 
            this.changeIntervalLabel.AutoSize = true;
            this.changeIntervalLabel.Location = new System.Drawing.Point(750, 606);
            this.changeIntervalLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.changeIntervalLabel.Name = "changeIntervalLabel";
            this.changeIntervalLabel.Size = new System.Drawing.Size(179, 20);
            this.changeIntervalLabel.TabIndex = 0;
            this.changeIntervalLabel.Text = "Change Interval (in Sec)";
            // 
            // randomCheckBox
            // 
            this.randomCheckBox.AutoSize = true;
            this.randomCheckBox.Location = new System.Drawing.Point(518, 605);
            this.randomCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.randomCheckBox.Name = "randomCheckBox";
            this.randomCheckBox.Size = new System.Drawing.Size(96, 24);
            this.randomCheckBox.TabIndex = 16;
            this.randomCheckBox.Text = "Random";
            this.toolTip.SetToolTip(this.randomCheckBox, "Wallpaper will be randomly selected when set");
            this.randomCheckBox.UseVisualStyleBackColor = true;
            // 
            // canRepeatRandomCheckBox
            // 
            this.canRepeatRandomCheckBox.AutoSize = true;
            this.canRepeatRandomCheckBox.Location = new System.Drawing.Point(518, 640);
            this.canRepeatRandomCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.canRepeatRandomCheckBox.Name = "canRepeatRandomCheckBox";
            this.canRepeatRandomCheckBox.Size = new System.Drawing.Size(186, 24);
            this.canRepeatRandomCheckBox.TabIndex = 17;
            this.canRepeatRandomCheckBox.Text = "Can Repeat Random";
            this.toolTip.SetToolTip(this.canRepeatRandomCheckBox, "Wallpapers can repeast when random is activated");
            this.canRepeatRandomCheckBox.UseVisualStyleBackColor = true;
            // 
            // removeWallpaper
            // 
            this.removeWallpaper.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.removeWallpaper.Location = new System.Drawing.Point(1110, 95);
            this.removeWallpaper.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.removeWallpaper.Name = "removeWallpaper";
            this.removeWallpaper.Size = new System.Drawing.Size(138, 35);
            this.removeWallpaper.TabIndex = 16;
            this.removeWallpaper.Text = "Remove current";
            this.toolTip.SetToolTip(this.removeWallpaper, "Remove current Wallpaper from list");
            this.removeWallpaper.UseVisualStyleBackColor = true;
            this.removeWallpaper.Click += new System.EventHandler(this.RemoveWallpaper_Click);
            // 
            // allFitButton
            // 
            this.allFitButton.Location = new System.Drawing.Point(696, 18);
            this.allFitButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allFitButton.Name = "allFitButton";
            this.allFitButton.Size = new System.Drawing.Size(112, 35);
            this.allFitButton.TabIndex = 11;
            this.allFitButton.Text = "All Fit";
            this.toolTip.SetToolTip(this.allFitButton, "Set all wallpapers to fit style");
            this.allFitButton.UseVisualStyleBackColor = true;
            this.allFitButton.Click += new System.EventHandler(this.SetAllStyles);
            // 
            // allFilledButton
            // 
            this.allFilledButton.Location = new System.Drawing.Point(696, 63);
            this.allFilledButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allFilledButton.Name = "allFilledButton";
            this.allFilledButton.Size = new System.Drawing.Size(112, 35);
            this.allFilledButton.TabIndex = 12;
            this.allFilledButton.Text = "All Filled";
            this.toolTip.SetToolTip(this.allFilledButton, "Set all wallpapers to filled style\r\nNO PREVIEW FOR FILLED STYLE");
            this.allFilledButton.UseVisualStyleBackColor = true;
            this.allFilledButton.Click += new System.EventHandler(this.SetAllStyles);
            // 
            // triggerTimeRadioButton
            // 
            this.triggerTimeRadioButton.AutoSize = true;
            this.triggerTimeRadioButton.Location = new System.Drawing.Point(18, 43);
            this.triggerTimeRadioButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerTimeRadioButton.Name = "triggerTimeRadioButton";
            this.triggerTimeRadioButton.Size = new System.Drawing.Size(68, 24);
            this.triggerTimeRadioButton.TabIndex = 1;
            this.triggerTimeRadioButton.TabStop = true;
            this.triggerTimeRadioButton.Text = "Time";
            this.toolTip.SetToolTip(this.triggerTimeRadioButton, "Trigger this event at a specific time");
            this.triggerTimeRadioButton.UseVisualStyleBackColor = true;
            this.triggerTimeRadioButton.CheckedChanged += new System.EventHandler(this.SelectTrigger);
            // 
            // triggerEventRadioButton
            // 
            this.triggerEventRadioButton.AutoSize = true;
            this.triggerEventRadioButton.Location = new System.Drawing.Point(18, 98);
            this.triggerEventRadioButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerEventRadioButton.Name = "triggerEventRadioButton";
            this.triggerEventRadioButton.Size = new System.Drawing.Size(75, 24);
            this.triggerEventRadioButton.TabIndex = 2;
            this.triggerEventRadioButton.TabStop = true;
            this.triggerEventRadioButton.Text = "Event";
            this.toolTip.SetToolTip(this.triggerEventRadioButton, "Trigger this event by an event");
            this.triggerEventRadioButton.UseVisualStyleBackColor = true;
            this.triggerEventRadioButton.CheckedChanged += new System.EventHandler(this.SelectTrigger);
            // 
            // triggerEventComboBox
            // 
            this.triggerEventComboBox.FormattingEnabled = true;
            this.triggerEventComboBox.Items.AddRange(new object[] {
            "Nightmode ON (Only Windows 10)",
            "Nightmode OFF (Only Windows 10)"});
            this.triggerEventComboBox.Location = new System.Drawing.Point(104, 98);
            this.triggerEventComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.triggerEventComboBox.Name = "triggerEventComboBox";
            this.triggerEventComboBox.Size = new System.Drawing.Size(368, 28);
            this.triggerEventComboBox.TabIndex = 6;
            this.triggerEventComboBox.Text = "Nightmode ON (Only Windows 10)";
            this.toolTip.SetToolTip(this.triggerEventComboBox, "Choose trigger event");
            // 
            // readMeLabel
            // 
            this.readMeLabel.AutoSize = true;
            this.readMeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readMeLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.readMeLabel.Location = new System.Drawing.Point(483, 105);
            this.readMeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.readMeLabel.Name = "readMeLabel";
            this.readMeLabel.Size = new System.Drawing.Size(114, 20);
            this.readMeLabel.TabIndex = 23;
            this.readMeLabel.Text = "READ THIS!";
            this.toolTip.SetToolTip(this.readMeLabel, "Click this - Important information about trigger evetns");
            this.readMeLabel.Click += new System.EventHandler(this.ReadMeLabel_Click);
            this.readMeLabel.MouseLeave += new System.EventHandler(this.UnHover);
            this.readMeLabel.MouseHover += new System.EventHandler(this.Hover);
            // 
            // AddWallpaperEventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1266, 709);
            this.Controls.Add(this.readMeLabel);
            this.Controls.Add(this.triggerEventComboBox);
            this.Controls.Add(this.triggerEventRadioButton);
            this.Controls.Add(this.triggerTimeRadioButton);
            this.Controls.Add(this.allFilledButton);
            this.Controls.Add(this.allFitButton);
            this.Controls.Add(this.removeWallpaper);
            this.Controls.Add(this.canRepeatRandomCheckBox);
            this.Controls.Add(this.randomCheckBox);
            this.Controls.Add(this.changeIntervalLabel);
            this.Controls.Add(this.changeIntervalUpDown);
            this.Controls.Add(this.allTiledButton);
            this.Controls.Add(this.allCenteredButton);
            this.Controls.Add(this.allStretchedButton);
            this.Controls.Add(this.styleLabel);
            this.Controls.Add(this.styleComboBox);
            this.Controls.Add(this.previewGroupBox);
            this.Controls.Add(this.noteTextBox);
            this.Controls.Add(this.noteLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.selectedWallpapersGroupBox);
            this.Controls.Add(this.selectFolderButton);
            this.Controls.Add(this.selectSingleImageButton);
            this.Controls.Add(this.triggerSecondSelector);
            this.Controls.Add(this.triggerMinuteSelector);
            this.Controls.Add(this.triggerHourSelector);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.triggerTimeLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AddWallpaperEventForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Wallpaper Event - Juckelfunks Wallaper Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddWallpaperEventForm_FormClosing);
            this.Shown += new System.EventHandler(this.AddWallpaperEventForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddWallpaperEventForm_KeyDown);
            this.selectedWallpapersGroupBox.ResumeLayout(false);
            this.previewGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeIntervalUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label triggerTimeLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox triggerHourSelector;
        private System.Windows.Forms.ComboBox triggerMinuteSelector;
        private System.Windows.Forms.ComboBox triggerSecondSelector;
        private System.Windows.Forms.Button selectSingleImageButton;
        private System.Windows.Forms.Button selectFolderButton;
        private System.Windows.Forms.Panel selectedWallpapersPanel;
        private System.Windows.Forms.GroupBox selectedWallpapersGroupBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label noteLabel;
        private System.Windows.Forms.TextBox noteTextBox;
        private System.Windows.Forms.GroupBox previewGroupBox;
        private System.Windows.Forms.PictureBox previewPictureBox;
        private System.Windows.Forms.ComboBox styleComboBox;
        private System.Windows.Forms.Label styleLabel;
        private System.Windows.Forms.Button allStretchedButton;
        private System.Windows.Forms.Button allCenteredButton;
        private System.Windows.Forms.Button allTiledButton;
        private System.Windows.Forms.NumericUpDown changeIntervalUpDown;
        private System.Windows.Forms.Label changeIntervalLabel;
        private System.Windows.Forms.CheckBox randomCheckBox;
        private System.Windows.Forms.CheckBox canRepeatRandomCheckBox;
        private System.Windows.Forms.Button removeWallpaper;
        private System.Windows.Forms.Button allFitButton;
        private System.Windows.Forms.Button allFilledButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.RadioButton triggerTimeRadioButton;
        private System.Windows.Forms.RadioButton triggerEventRadioButton;
        private System.Windows.Forms.ComboBox triggerEventComboBox;
        private System.Windows.Forms.Label readMeLabel;
    }
}