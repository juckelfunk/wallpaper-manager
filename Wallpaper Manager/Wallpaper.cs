﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

/**
 * Title: Wallpaper
 * Author: Neil N, Juckelfunk
 * Date: 23.05.2018 | 22:31 CET
 * Desc: A wallpaper that can be set. Source code is from stackoverflow but was edited by me.
 * Source: https://stackoverflow.com/questions/1061678/change-desktop-wallpaper-using-code-in-net (24.12.2017)
 */

namespace Wallpaper_Manager
{
    public class Wallpaper
    {
        const int SPI_SETDESKWALLPAPER = 20;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDWININICHANGE = 0x02;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
        
        public readonly string imagePath;     // The path to the image file
        public Style ImageStyle { get; set; } // The image style

        /// <summary>
        /// Create a new wallpaper
        /// </summary>
        /// <param name="path">The path to the image file</param>
        /// <param name="imageStyle">The style in which the wallpaper will be allplied to the desktop</param>
        public Wallpaper(string path, Style imageStyle)
        {
            imagePath = path;
            ImageStyle = imageStyle;
        }

        /// <summary>
        /// Styles in which the wallpaper will be applied to the desktop
        /// </summary>
        public enum Style : int
        {
            Tiled,
            Centered,
            Stretched,
            Fit,
            Filled
        }

        /// <summary>
        /// Set the users wallpaper
        /// </summary>
        /// <param name="img">The image that will be applied</param>
        /// <param name="style">The style how the image will be applied</param>
        public void Set()
        {
            Program.Logger.Info("Set wallpaper: " + ToString());
            try
            {
                Image image = Image.FromFile(imagePath);

                string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.bmp");
                image.Save(tempPath, System.Drawing.Imaging.ImageFormat.Bmp);

                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);

                switch (ImageStyle)
                {
                    case Style.Stretched:
                        key.SetValue(@"WallpaperStyle", "2");
                        key.SetValue(@"TileWallpaper", "0");
                        break;

                    case Style.Centered:
                        key.SetValue(@"WallpaperStyle", "1");
                        key.SetValue(@"TileWallpaper", "0");
                        break;

                    case Style.Tiled:
                        key.SetValue(@"WallpaperStyle", "1");
                        key.SetValue(@"TileWallpaper", "1");
                        break;

                    case Style.Fit:
                        key.SetValue(@"WallpaperStyle", "6");
                        key.SetValue(@"TileWallpaper", "0");
                        break;

                    case Style.Filled:
                        key.SetValue(@"WallpaperStyle", "10");
                        key.SetValue(@"TileWallpaper", "0");
                        break;
                }

                // Set wallpaper
                SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, tempPath, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);

                // Unload the image from ram because we don't need it anymore after settings it as wallpaper
                image.Dispose();
            }
            catch (Exception e)
            {
                Utils.ShowErrorMessage("Error while setting wallpaper: " + ToString() + ": " + e);
            }
        }

        /// <summary>
        /// Returns a string representing this wallpaper
        /// </summary>
        /// <returns>A nicely formated string which represents this wallpapers image file path and image style</returns>
        public override string ToString()
        {
            string res = "";

            res += "Path: " + imagePath + " | ";
            res += "Style: " + ImageStyle;

            return res;
        }
    }
}
