﻿using System;
using System.Drawing;
using System.Windows.Forms;

/*
 * Title: Wallpaper Event Button
 * Author: Juckelfunk
 * Date: 27.05.2018 | 12:56
 * Desc: A button that show all relevant information about a Wallpaper event.
 *       It contains a button which will lead to an add wallpaper event form where the user can adjust the settings,
 *       a preview image which can be set to fullscreen witch the Show Image Form
 *       3 buttons where the user can delete the event, view information and start the event immidiatly
 */

namespace Wallpaper_Manager
{
    public partial class WallpaperEventButton : UserControl
    {
        public WallpaperEvent WallpaperEvent { get; set; }  // Wallpaper event that is represented by this button

        /* These events will be triggered when the user clicks the control buttons so that the main form can handle the input */
        public event EventHandler StartNowEvent;            // Start now button
        public event EventHandler StopServiceEvent;         // Remove button
        public event EventHandler SaveEvent;                // Remove, Activation and edit button
        public event EventHandler DuplicateEvent;           // Duplicate button

        /// <summary>
        /// Create a wallpaper button which represents a wallapper event
        /// </summary>
        /// <param name="wallpaperEvent">The wallpaper event that will be represented</param>
        public WallpaperEventButton(WallpaperEvent wallpaperEvent)
        {
            InitializeComponent();

            this.WallpaperEvent = wallpaperEvent;
            UpdateControls();
        }

        /// <summary>
        ///  Loads the preview image of the first wallpaper in the wallpaper event list
        /// </summary>
        public void LoadPreview()
        {
            // If turned off, we won't show a preview
            if (!Settings.Default.Previews)
                return;

            if (this.WallpaperEvent.Wallpapers.Count != 0)
            {
                Image image;
                try
                {
                    image = Image.FromFile(this.WallpaperEvent.Wallpapers[0].imagePath);
                }
                catch (Exception e)
                {
                    Utils.ShowErrorMessage("Could not load preview image of wallpaper event '" + WallpaperEvent.Note + "': " + e.Message);
                    return;
                }
                Bitmap bmp = Utils.ResizeImage(image, pictureBox.Width, pictureBox.Height);
                pictureBox.Image = bmp;
                image.Dispose();
            }
        }

        /// <summary>
        /// Update the information shown on the edit wallpaper event button
        /// </summary>
        private void UpdateControls()
        {
            LoadPreview();

            /* Button text */

            // Note
            string text = "";
            if (String.IsNullOrEmpty(WallpaperEvent.Note))
                text = "New Event\n";
            else
                text = this.WallpaperEvent.Note + "\n";

            // Trigger event / time
            if (WallpaperEvent.UseTriggerEvent)
            {
                switch (WallpaperEvent.TriggerEvent)
                {
                    case WallpaperEvent.TriggerEvents.Nightmode_ON:
                        text += "Event: Nightmode ON\n";
                        break;
                    case WallpaperEvent.TriggerEvents.Nightmode_OFF:
                        text += "Event: Nightmode OFF\n";
                        break;
                }
            }
            else
            {
                text += "Time: " + this.WallpaperEvent.Time.ToLongTimeString() + "\n";
            }

            // Rest of the information
            text += "Random: " + this.WallpaperEvent.Random + "\n"
                + "Can repeat Random: " + this.WallpaperEvent.CanRepeatRandom + "\n"
                + "Inner change time: " + (this.WallpaperEvent.InnerChangeTime / 1000) + "\n"
                + "Wallpapers Count: " + this.WallpaperEvent.Wallpapers.Count;

            button.Text = text;

            UpdateActivation();
        }

        /// <summary>
        /// Updates the user UI according to the wallpaperevents activation
        /// </summary>
        private void UpdateActivation()
        {
            if (WallpaperEvent.Activated)
            {
                activationButton.Text = "✗";
                toolTip.SetToolTip(activationButton, "Deactivate event");
                BackColor = SystemColors.ControlDark;       
            }
            else
            {
                activationButton.Text = "✓";
                toolTip.SetToolTip(activationButton, "Activate event");
                BackColor = Color.DarkRed;
            }
        }

        /// <summary>
        /// Let the user edit this wallpaper event with the addWallpaperEventForm
        /// </summary>
        private void Button_Click(object sender, EventArgs e)
        {
            AddWallpaperEventForm addForm = new AddWallpaperEventForm(this.WallpaperEvent);
            addForm.ShowDialog();
            if (addForm.DialogResult == DialogResult.OK)
            {
                // Set the changed wallpaper event as the current one of this button and update the controls
                this.WallpaperEvent = addForm.EventResult;
                UpdateControls();

                // Save events with the MainForm event
                SaveEvent?.Invoke(this, EventArgs.Empty);

                // BUG: For some reason the preview image is not disposed by the form closing event.
                // Currently this is the only way i can fix that. Maybe i find a better solution later.
                GC.Collect();
            }
        }

        /// <summary>
        /// Remove this wallpaper event form the list
        /// </summary>
        private void RemoveButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you really want to delete this event and save?\n",
                "Delete Event", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return;

            // To make sure that if the event is currently running it is stopped before deleting
            StopServiceEvent?.Invoke(this, EventArgs.Empty);

            // Regenerate panel so that there is no space in between the remaining buttons
            ControlCollection controls = this.Parent.Controls;
            controls.Remove(this);
            int i = 0;
            foreach (Control c in controls)
            {
                c.Location = new Point(4, 4 + i * (c.Size.Height + 4));
                i++;
            }

            // Remove the uneeded preview image from ram
            if (pictureBox.Image != null)
                pictureBox.Image.Dispose();

            // Save the changes
            SaveEvent?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Show the user information about this wallpaper event in text form
        /// </summary>
        private void InfoButton_Click(object sender, EventArgs e)
        {
            InfoTextForm form = new InfoTextForm(WallpaperEvent.ToString());
            form.ShowDialog();
        }

        /// <summary>
        /// Starts this wallpaper event imidiatly by an event in the MainForm
        /// </summary>
        private void PlayNowButton_Click(object sender, EventArgs e)
        {
            StartNowEvent?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Show the user a fullscreen preview of the first image in the wallpaper event list according to the image style
        /// </summary>
        private void PictureBox_Click(object sender, EventArgs e)
        {
            if (WallpaperEvent.Wallpapers.Count == 0)
                return;
            ShowImageForm form = new ShowImageForm(WallpaperEvent.Wallpapers[0]);
            form.ShowDialog();
        }

        /// <summary>
        /// Set the current cursor to a hand informing the user that he can click something
        /// </summary>
        private void Hover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        /// <summary>
        /// Set the cursor back to normal
        /// </summary>
        private void UnHover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Arrow;
        }

        /// <summary>
        /// Activate / Deactivate the event. The event will not be started if it is not activated
        /// </summary>
        private void ActivationButton_Click(object sender, EventArgs e)
        {
            if (WallpaperEvent.Activated)
            {
                WallpaperEvent.Activated = false;
                WallpaperEvent.Stop();
            }
            else
            {
                WallpaperEvent.Activated = true;
            }
            UpdateActivation();

            // Save so that next time when started the activation is applied again
            SaveEvent?.Invoke(this, EventArgs.Empty);
        }

        private void DuplicateButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Do you want to duplicate this event?", "Duplicate event", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No)
                return;
            DuplicateEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
