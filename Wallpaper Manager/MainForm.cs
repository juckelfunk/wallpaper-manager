﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Timer = System.Threading.Timer;

/**
 * Title: Main Form
 * Author: Juckelfunk
 * Date: 23.05.2018 | 22:01 CET
 * Desc: Main Form of this application
 */

namespace Wallpaper_Manager
{
    public partial class MainForm : Form
    {
        public const string END_OF_EVENT = "////////////////////////////////"; // The string that is used to see where a new wallpaperEvent is started in the save file
        // The registry paths that are used to monitor the nightmode. More information in the help file (./Resources/help.txt)
        private const string NIGHTMODE_REG_PATH = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\DefaultAccount\Cloud\default$windows.data.bluelightreduction.bluelightreductionstate\windows.data.bluelightreduction.bluelightreductionstate";
        private const string NIGHTMODE_SETTINGS_PATH = @"HKEY_CURRENT_USER\Control Panel\Quick Actions\Control Center\QuickActionsStateCapture";
        private const string NIGHTMODE_QUICKACTION_BUTTON = "Microsoft.QuickAction.BlueLightReduction";

        private List<Timer> timers = new List<Timer>(); // All timers that trigger the wallpaper events by time
        private bool close = false;                     // True when the user has choosen to close the application and not minimize.
        private bool running = false;                   // Determine if the service is running
        private bool startMinimized;                    // Start the application minimized (By startup e.g.) so don't show form when start
        private bool nightmodeOn = false;               // Determine if the nightmode is turned on
        private bool ignoreActivation = false;          // Temp bool to check if the StartEvent method should ignore the events activation (for startEvent button)

        private RegistryMonitor nightmodeMonitor;       // The registry monitor to check if the registry key that changes when the nightmode is changed
        private static int WM_QUERYENDSESSION = 0x11;   // This is an event called by the system to detect if the user shuts down windows or logs off (used for WndProc method)

        public MainForm(bool startMinimized)
        {
            this.startMinimized = startMinimized;
            Program.Logger.Info("Start minimized: " + startMinimized);

            InitializeComponent();

            /* For more information about how I check the nightmode, read the help file (./Resources/help.txt) */

            // Get initial nightmode
            string temp = Registry.GetValue(NIGHTMODE_SETTINGS_PATH, "Toggles", (object)"false").ToString();
            if (temp.Contains(NIGHTMODE_QUICKACTION_BUTTON + ":false"))
                nightmodeOn = false;
            else if (temp.Contains(NIGHTMODE_QUICKACTION_BUTTON + ":true"))
                nightmodeOn = true;
            Program.Logger.Info("Initial nightmode: " + nightmodeOn);

            // Start nightmode registry monitor
            nightmodeMonitor = new RegistryMonitor(NIGHTMODE_REG_PATH);
            nightmodeMonitor.RegChanged += new EventHandler(OnNightmodeChanged);
            nightmodeMonitor.Start();
            
            // Set up stuff for minimized start
            if (startMinimized)
            {
                notifyIcon.Visible = true;
                LoadEvents(Settings.Default.CurrentPath);
                StartService();
                if (Settings.Default.MessageOnStartup)
                {
                    notifyIcon.ShowBalloonTip(2000, "Juckelfunks Wallpaper Manager", "Wallpaper manager has been started." +
                        " If you don't want this message to show on startup, change the setting in the wallpapaer manager settings.", ToolTipIcon.Info);
                }
            }
        }

        /// <summary>
        /// This method will be called when the user shuts down windows or is logging off
        /// Then we don't want the wallpaper manager to ask if the user wants to close the application so we set close to true
        /// Then wallpaper manager will not ask if the user really wants to close the application
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_QUERYENDSESSION)
            {
                Program.Logger.Info("User shuts down windows or logs off");
                close = true;
            }

            // If this is WM_QUERYENDSESSION, the closing event should be
            // raised in the base WndProc.
            base.WndProc(ref m);
        }

        private void RestartApplication()
        {
            Program.Logger.Info("Restart Application");
            close = true;
            Settings.Default.RestartMinimized = WindowState == FormWindowState.Minimized;
            Settings.Default.AllowMultipleInstances = true;
            Settings.Default.Save();
            Application.Restart();
        }

        /// <summary>
        /// Called when nighmode is changed.
        /// Update the nightmode bool, start events that are triggered by an nightmode event.
        /// </summary>
        private void OnNightmodeChanged(object sender, EventArgs e)
        {
            nightmodeOn = !nightmodeOn;

            Program.Logger.Info("Nightmode changed: " + nightmodeOn);
            if (running)
                StartByEventTrigger();
        }
        
        /// <summary>
        /// Add an wallpaper event to the list
        /// </summary>
        /// <param name="wallpaperEvent">The wallpaper event that will be added</param>
        public void AddEvent(WallpaperEvent wallpaperEvent)
        {
            Program.Logger.Info("Add new wallpaper event:\n" + wallpaperEvent);
            
            WallpaperEventButton button = new WallpaperEventButton(wallpaperEvent);
            button.Size = new Size(eventPanel.Width - 20, button.Height);
            button.Location = new Point(0, 4 + eventPanel.Controls.Count * (button.Size.Height + 4));
            button.StartNowEvent += new EventHandler(StartEvent);
            button.StopServiceEvent += new EventHandler(StopServiceEvent);
            button.SaveEvent += new EventHandler(SaveEvents);
            button.DuplicateEvent += new EventHandler(DuplicateEvent);
            eventPanel.Controls.Add(button);
        }

        /// <summary>
        /// Load events from the given file
        /// TODO: Make this method more compact!
        /// </summary>
        /// <param name="path">The file from which the events will be loaded</param>
        public void LoadEvents(string path)
        {
            bool error = false;

            if (!File.Exists(path))
            {
                Program.Logger.Error("Loading file does not exist: Exiting... | " + path);
                return;
            }
            else
            {
                Program.Logger.Info("Load events from file: " + path);
            }

            // Set the title to loading
            string originalText = Text;
            Text = Text + " - Loading";

            // Stop all currently running events so we can unload all events without having them running as zombies
            StopService();

            StreamReader reader = null;
            try
            {
                reader = new StreamReader(path);
                int lineCounter = -1;

                string note = "";
                DateTime time = DateTime.Now;
                WallpaperEvent.TriggerEvents triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_ON;
                bool useTriggerEvent = false;
                bool random = false;
                bool canRepeatRandom = false;
                bool activated = false;
                int innerChangeTime = 0;
                List<Wallpaper> wallpapers = new List<Wallpaper>();

                // For each line
                while (true)
                {
                    string line = reader.ReadLine();
                    lineCounter++;

                    // End of file
                    if (line == null)
                        break;

                    /* Build event */
                    if (line.StartsWith("Note: "))
                        note = line.Substring(6);
                    else if (line.StartsWith("Time: "))
                        time = DateTime.Parse(line.Substring(6));
                    else if (line.StartsWith("Random: "))
                        random = Boolean.Parse(line.Substring(8));
                    else if (line.StartsWith("CanRepeatRandom: "))
                        canRepeatRandom = Boolean.Parse(line.Substring(17));
                    else if (line.StartsWith("InnerChangeTime: "))
                        innerChangeTime = Int32.Parse(line.Substring(17));
                    else if (line.StartsWith("UseTriggerEvent: "))
                        useTriggerEvent = Boolean.Parse(line.Substring(17));
                    else if (line.StartsWith("Activated: "))
                        activated = Boolean.Parse(line.Substring(11));
                    else if (line.StartsWith("Event: "))
                    {
                        string eventString = line.Substring(7);
                        switch (eventString)
                        {
                            case "Nightmode_ON":
                                triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_ON;
                                break;
                            case "Nightmode_OFF":
                                triggerEvent = WallpaperEvent.TriggerEvents.Nightmode_OFF;
                                break;
                        }
                    }
                    else if (line.StartsWith("    Path: "))
                    {
                        // Path of new wallpaper
                        int pathEnd = line.IndexOf("|") - 1;
                        int length = pathEnd - 10; // | - "    Path: "
                        string wallpaperPath = line.Substring(10, length);
                        if (!File.Exists(wallpaperPath))
                        {
                            Program.Logger.Error("New Wallpaper path '" + wallpaperPath + "' does not exist. Continuing...");
                            error = true;
                            continue;
                        }

                        // Style of new wallpaper
                        string styleString = line.Substring(pathEnd + 10); // | + " Style: "
                        Wallpaper.Style style = Wallpaper.Style.Centered;

                        bool styleExists = true;
                        switch (styleString)
                        {
                            case "Tiled":
                                style = Wallpaper.Style.Tiled;
                                break;
                            case "Centered":
                                style = Wallpaper.Style.Centered;
                                break;
                            case "Stretched":
                                style = Wallpaper.Style.Stretched;
                                break;
                            case "Fit":
                                style = Wallpaper.Style.Fit;
                                break;
                            case "Filled":
                                style = Wallpaper.Style.Filled;
                                break;
                            default:
                                Utils.ShowErrorMessage("Could not load Wallpaper: Style " + styleString + " does not exist!");
                                styleExists = false;
                                error = true;
                                break;
                        }
                        if (!styleExists)
                            continue;

                        wallpapers.Add(new Wallpaper(wallpaperPath, style));
                    }
                    // Wallpaper event is ready
                    else if (line == END_OF_EVENT)
                    {
                        WallpaperEvent wallpaperEvent = new WallpaperEvent(
                            note,
                            time,
                            triggerEvent,
                            useTriggerEvent,
                            wallpapers,
                            random,
                            canRepeatRandom,
                            innerChangeTime,
                            activated
                        );
                        AddEvent(wallpaperEvent);
                        wallpapers = new List<Wallpaper>();
                    }
                    else
                    {
                        // Ignored lines
                        if (line.StartsWith("Index: ")
                            || line.StartsWith("Wallpapers:"))
                            continue;

                        // Empty lines
                        if (String.IsNullOrEmpty(line))
                            continue;

                        // Comments
                        string lineWithoutStartSpaces = line;
                        while (lineWithoutStartSpaces[0] == ' ')
                            lineWithoutStartSpaces.Remove(0, 1);
                        if (lineWithoutStartSpaces.StartsWith("#"))
                            continue;

                        // Invalid line
                        throw new InvalidLineException("Invalid line in loading file: '" + path + "'\n" +
                            "\nLine is: '" + line + "'" +
                            "\nLine number: " + (lineCounter + 1));
                    }
                }
            }
            catch (Exception e)
            {
                Program.Logger.Error("An error occured while loading events from file '" + path + "'\n" + e);
                error = true;
                return;
            }
            finally
            {
                if (reader.BaseStream != null)
                    reader.Close();

                // Set title to normal
                Text = originalText;

                if (error)
                    Utils.ShowErrorMessage("An error occured while loading events from file '" + path + "'! See the console for more information.");
            }
        }

        /// <summary>
        /// Called from a wallpaperEventaButton when activation button is pressed
        /// </summary>
        private void SaveEvents(object sender, EventArgs e)
        {
            SaveEvents(Settings.Default.CurrentPath);
        }

        /// <summary>
        /// Save all events in current list to a file
        /// </summary>
        /// <param name="path">The file in which the events will be saved</param>
        public void SaveEvents(string path)
        {
            Program.Logger.Info("Save current events to " + path);

            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path);

                foreach (Control c in eventPanel.Controls)
                {
                    if (c.GetType() == typeof(WallpaperEventButton))
                    {
                        WallpaperEventButton button = c as WallpaperEventButton;
                        WallpaperEvent currentEvent = button.WallpaperEvent;
                        Program.Logger.Info("Save event: '" + currentEvent.Note + "'");
                        writer.WriteLine(currentEvent);
                        writer.WriteLine(END_OF_EVENT);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.ShowErrorMessage("Could not save events: " + e);
            }
            finally
            {
                if (writer.BaseStream != null)
                    writer.Close();
            }
        }

        /// <summary>
        /// Minimizes the MainForm to the system tray
        /// </summary>
        private void MinimizeToSystemTray()
        {
            Program.Logger.Info("Minimize to system tray");

            // Check if the cursor is in the taskbar. If so, we don't want to minimize to the system tray, because that would be unexcpected windows behaviour
            bool cursorNotInBar = Screen.GetWorkingArea(this).Contains(Cursor.Position);
            if (this.WindowState == FormWindowState.Minimized && cursorNotInBar)
            {
                // Delete preview images from memory. We will rebuild them when the form is reloaded again
                foreach (WallpaperEventButton c in eventPanel.Controls)
                {
                    WallpaperEventButton button = c as WallpaperEventButton;
                    if (button.pictureBox.Image != null)
                        button.pictureBox.Image.Dispose();
                }

                this.ShowInTaskbar = false;
                notifyIcon.Visible = true;
                this.Hide();
                Program.Hide = true;
                if (Settings.Default.MesageOnTray)
                {
                    notifyIcon.ShowBalloonTip(2000, "Juckelfunks Wallpaper Manager",
                    "The wallpaper manager is now minimized to the system tray and will run in the backgound.\n" +
                    "Click the notify icon to show again", ToolTipIcon.Info);
                }
            }
        }

        /// <summary>
        /// Recover from system tray
        /// </summary>
        private void RecoverFromTray()
        {
            Program.Logger.Info("Recover from system tray");

            this.ShowInTaskbar = true;
            notifyIcon.Visible = false;
            this.Show();
            WindowState = FormWindowState.Normal;

            // Load previews
            foreach (WallpaperEventButton c in eventPanel.Controls)
            {
                WallpaperEventButton button = c as WallpaperEventButton;
                button.LoadPreview();
            }
        }

        /// <summary>
        /// Initialize all events by starting timers
        /// </summary>
        private void InitEvents()
        {
            if (Controls.Count == 0)
                return;

            List<Tuple<WallpaperEvent, int>> tuples = new List<Tuple<WallpaperEvent, int>>(); // Use this to see which event should be triggered if we start the last event
            foreach (WallpaperEventButton c in eventPanel.Controls)
            {
                WallpaperEventButton button = c as WallpaperEventButton;
                WallpaperEvent we = button.WallpaperEvent;

                // This event will not be triggered by time but by an event, so don't start timer on this event
                if (we.UseTriggerEvent)
                    continue;

                // Calculate timespan until first start
                we.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                    we.Time.Hour, we.Time.Minute, we.Time.Second);
                DateTime time = we.Time;
                if (time < DateTime.Now)
                    time = time.AddDays(1);
                TimeSpan span = time - DateTime.Now;

                // Wait until first event will start and after that repeat every 24 hours (864...) in ms
                int ticks = (int)span.TotalMilliseconds;
                Timer timer = new Timer(StartEvent, we, ticks, 86400000);
                timers.Add(timer);
                tuples.Add(new Tuple<WallpaperEvent, int>(we, ticks));
            }

            // Start last event that would have been triggered if the service would have run,
            // but only if last event is triggered by time and not by event
            if (Settings.Default.StartLastEvent &&
                !Settings.Default.PreferEventOverTime)
            {
                // Add the ticks until the event is triggered to a list
                List<int> ints = new List<int>();
                foreach (var tuple in tuples)
                {
                    if (tuple.Item1.UseTriggerEvent)
                        continue;
                    ints.Add(tuple.Item2);
                }

                if (ints.Count == 0)
                    return;

                // Find event with the most ticks until triggered the next time. This will be the event that would have been triggered last
                int mostTicks = ints.Max();

                // Find the event with the most ticks and start it
                foreach (var tuple in tuples)
                {
                    if (tuple.Item2 == mostTicks)
                    {
                        Program.Logger.Info("Start last event that would have been triggered");
                        StartEvent(tuple.Item1);
                        break;
                    }
                }
            }

            // Start last event by trigger event
            else if (Settings.Default.StartLastEvent
                && Settings.Default.PreferEventOverTime)
            {
                StartByEventTrigger();
            }
        }

        /// <summary>
        /// Start the event of a wallpaperEventButton
        /// </summary>
        private void StartEvent(object sender, EventArgs e)
        {
            WallpaperEventButton button = sender as WallpaperEventButton;
            ignoreActivation = true;
            StartEvent(button.WallpaperEvent);
            ignoreActivation = false;
        }

        /// <summary>
        /// Start an wallpaperEvent
        /// Also used to stop all events (newEvent will be null)
        /// </summary>
        private void StartEvent(object newEvent)
        {
            WallpaperEvent we = newEvent as WallpaperEvent;

            // Disable all other events and enable the current one
            foreach (WallpaperEventButton c in eventPanel.Controls)
            {
                WallpaperEventButton button = c as WallpaperEventButton;

                if (button.WallpaperEvent == we && (button.WallpaperEvent.Activated || ignoreActivation))
                {
                    // First stop the event so that the event won't be running twice (just for savety=
                    button.WallpaperEvent.Stop();
                    button.BackColor = Color.DarkGreen;
                    button.WallpaperEvent.Start();
                }
                else
                {
                    if (button.WallpaperEvent.Activated)
                        button.BackColor = SystemColors.ControlDark;
                    else
                        button.BackColor = Color.DarkRed;
                    button.WallpaperEvent.Stop();
                }
            }
        }

        /// <summary>
        /// Goes through everey event and start it when an event is true
        /// </summary>
        private void StartByEventTrigger()
        {
            foreach (WallpaperEventButton c in eventPanel.Controls)
            {
                WallpaperEventButton button = c as WallpaperEventButton;
                WallpaperEvent we = button.WallpaperEvent;

                // If the event uses a trigger, check if the event was triggered
                if (we.UseTriggerEvent)
                {
                    if (we.TriggerEvent == WallpaperEvent.TriggerEvents.Nightmode_ON
                        && nightmodeOn)
                    {
                        StartEvent(we);
                        return;
                    }
                    else if (we.TriggerEvent == WallpaperEvent.TriggerEvents.Nightmode_OFF
                        && !nightmodeOn)
                    {
                        StartEvent(we);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Will be called by the duplicateEvent of a WallpaperEventButton
        /// Duplicate an event and add it to the bottom of the list.
        /// Saves automatically
        /// </summary>
        /// <param name="sender">The WallpaperEventButton from where the event is coming from</param>
        private void DuplicateEvent(object sender, EventArgs e)
        {
            if (sender.GetType() != typeof(WallpaperEventButton))
            {
                Utils.ShowErrorMessage("An intern error has occurred! Cannot duplicate event. Incoming sender was no WallpaperEventButton.");
                return;
            }
            WallpaperEventButton button = sender as WallpaperEventButton;
            Program.Logger.Info("Duplicate Event '" + button.WallpaperEvent.Note + "'");
            WallpaperEvent we = button.WallpaperEvent;
            we.Note = we.Note + " - Copy";
            AddEvent(we);
            SaveEvents(Settings.Default.CurrentPath);
        }

        /// <summary>
        /// Set next wallpaper according to the list. Only works when service is running
        /// </summary>
        private void NextWallpaper()
        {
            if (!running)
                return;
            // Go through each event and find the currently running, then set the next wallpaper
            foreach (Control button in eventPanel.Controls)
            {
                if (button.GetType() == typeof(WallpaperEventButton))
                {
                    WallpaperEventButton wallpaperEventButton = button as WallpaperEventButton;
                    WallpaperEvent wallpaperEvent = wallpaperEventButton.WallpaperEvent;

                    if (wallpaperEvent.running)
                    {
                        wallpaperEvent.NextWallpaper();
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Start the background service
        /// </summary>
        private void StartService()
        {
            InitEvents();
            running = true;
            serviceStatusLabel.Text = "Service: Started";
            startStopServiceMenuItem.Checked = true;
            Program.Logger.Info("Service started");
        }

        /// <summary>
        /// Stop the background service
        /// </summary>
        private void StopService()
        {
            // Disable all events (null so that no event is started)
            StartEvent(null);

            // Dispose timers
            foreach (Timer timer in timers)
                timer.Dispose();
            timers = new List<Timer>();
            running = false;

            serviceStatusLabel.Text = "Service: Stopped";
            startStopServiceMenuItem.Checked = false;
            Program.Logger.Info("Service stopped");
        }

        #region UI Controls

        #region Form Behaviour

        /// <summary>
        /// Recover from system tray
        /// </summary>
        private void NotifyIcon_DoubleMouseClick(object sender, MouseEventArgs e)
        {
            RecoverFromTray();
        }

        /// <summary>
        /// Ask the user if he really wants to close the application or minimize it to system tray
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If the user already said that he wants to close the application don't ask him again.
            // We need this because the formClosing event would be called again despite the user already answered the question
            // Also true when user shuts down windows or logs off
            if (close)
                return;

            if (!Settings.Default.AskBeforeClosing)
            {
                close = true;
                Program.Logger.Info("Close application");
                this.Close();
                Environment.Exit(0);
            }

            ConfirmForm form = new ConfirmForm("When you close this window, the service will NOT run! " +
                "But you can minimize this window to the system tray. The application will then run in the background.",
                "Minimize", "Close", "Cancel");
            form.ShowDialog();

            switch (form.Result)
            {
                case "Minimize":
                    WindowState = FormWindowState.Minimized;
                    MinimizeToSystemTray();
                    e.Cancel = true;
                    break;
                case "Close":
                    close = true;
                    Program.Logger.Info("Close application");
                    Close();
                    Environment.Exit(0);
                    break;
                case "Cancel":
                    e.Cancel = true;
                    break;
                default:
                    e.Cancel = true;
                    MessageBox.Show("Unexcpected answer: Will not close!");
                    break;
            }
        }

        /// <summary>
        /// Load events if the application was not started by startup.
        /// This is because the form will not show if we start by startup. But the events were loaded before.
        /// So we don't load them twice
        /// </summary>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (!startMinimized)
                LoadEvents(Settings.Default.CurrentPath);
        }

        /// <summary>
        /// If the MainForm is resized it may be minimized, so call the systemTray method
        /// </summary>
        private void MainForm_Resize(object sender, EventArgs e)
        {
            MinimizeToSystemTray();
        }

        #endregion Form Behaviour

        #region Buttons

        /// <summary>
        /// Save events and maybe restart the background service
        /// </summary>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            // Save changes
            SaveEvents(Settings.Default.CurrentPath);

            // Restart service if already running
            if (running)
            {
                stopServiceButton.PerformClick();
                startServiceButton.PerformClick();
            }
        }

        /// <summary>
        /// Show the addWallpaperEventForm and maybe add the new wallpaperEvent to the current list
        /// </summary>
        private void AddEventButton_Click(object sender, EventArgs e)
        {
            // Add wallpaper event
            AddWallpaperEventForm addForm = new AddWallpaperEventForm();
            addForm.ShowDialog();
            if (addForm.DialogResult == DialogResult.OK)
            {
                AddEvent(addForm.EventResult);
                SaveEvents(Settings.Default.CurrentPath);
                // BUG: For some reason the preview image is not disposed by the form closing event.
                // Currently this is the only way i can fix that. Maybe i find a better solution later.
                GC.Collect();
            }
        }

        /// <summary>
        /// Will be called by an wallpaperEventButton:
        /// To make sure service is stopped before deleting the event
        /// </summary>
        private void StopServiceEvent(object sender, EventArgs e)
        {
            StopService();
        }

        private void StopServiceButton_Click(object sender, EventArgs e)
        {
            StopService();
        }

        private void StartServiceButton_Click(object sender, EventArgs e)
        {
            StartService();
        }

        /// <summary>
        /// Apply the changes and minimize to system tray
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            applyButton.PerformClick();
            WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Delete all current events if the user confirms.
        /// Also stop the background service before so we have no wallpaperEvent zombies.
        /// </summary>
        private void DeleteAllButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Do you really want to delete every event?", "Delete all events", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == DialogResult.Cancel)
                return;
            StopService();
            eventPanel.Controls.Clear();
        }
        
        private void NextButton_Click(object sender, EventArgs e)
        {
            NextWallpaper();
        }

        #endregion Buttons

        #region Tool Strip

        private void SettingsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SettingsForm form = new SettingsForm();
            form.ShowDialog();
            // If we need to reload the previews because of a change in settings that the user made, do that
            if (form.ResultParameters.Contains("RELOAD_PREVIEWS"))
                reloadEventsToolStripMenuItem.PerformClick();
        }

        private void ApplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            applyButton.PerformClick();
        }

        /// <summary>
        /// Load events from a file that the user has choosen
        /// </summary>
        private void LoadEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            DialogResult res = file.ShowDialog();
            if (String.IsNullOrEmpty(file.FileName)
                || res == DialogResult.Cancel)
                return;
            eventPanel.Controls.Clear();
            LoadEvents(file.FileName);
            Settings.Default.CurrentPath = file.FileName;
            Settings.Default.Save();
        }

        /// <summary>
        /// Export all current events to a file which the user has choosen
        /// </summary>
        private void ExportEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog();
            file.ShowDialog();
            if (String.IsNullOrEmpty(file.FileName))
                return;
            SaveEvents(file.FileName);
        }

        /// <summary>
        ///  Reload all events from current file
        /// </summary>
        private void ReloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eventPanel.Controls.Clear();
            LoadEvents(Settings.Default.CurrentPath);
        }

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void MinimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Show some debug information in text form
        /// BUG: Some resource is not disposed so that unnecessary RAM is used after dialog is closed
        /// </summary>
        private void DebugInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string info = "";

            // Show current version
            info += "Version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + "\n\n\n";
            // Show current settings
            info += Utils.SettingsToString() + "\n\n";
            // Show current time
            info += "Current time: " + DateTime.Now + "\n\n\n";
            // Show current events
            foreach (Control c in eventPanel.Controls)
            {
                if (c.GetType() == typeof(WallpaperEventButton))
                {
                    WallpaperEventButton button = c as WallpaperEventButton;
                    info += button.WallpaperEvent;
                    info += "\n" + END_OF_EVENT + "\n\n";
                }
            }
            // Log file (I know this code is copy paste from the logger class. But i don't care right now...)
            string loggerId = Program.loggerID;
            var date = DateTime.Now.ToString("yyyyMMdd");
            var logFileName = Settings.Default.LogPath + "\\" + string.Format(string.IsNullOrEmpty(loggerId) ? "{0}.log" : "{0}_{1}.log", date, loggerId);
            string fileContent = "";
            try
            {
                if (File.Exists(logFileName))
                    fileContent = File.ReadAllText(logFileName);
                else
                    fileContent = "File does not exist: '" + logFileName + "' Logging might not be enabled!";
            }
            catch (Exception exc)
            {
                Program.Logger.Error("Could not read log file for debugging info: " + exc.Message);
                fileContent = "Could not read log file for debugging info: " + exc.Message;
            }
            // We don't want to log the log file itself. So log all the information without the log file content. This will only be shown in the textbox
            Program.Logger.Debug("Debug information:\n" + info);
            info += "\nLog file:\n\n\n";
            info += fileContent;

            InfoTextForm form = new InfoTextForm(info);
            form.ShowDialog();
        }

        private void StopServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stopServiceButton.PerformClick();
        }

        private void StartServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startServiceButton.PerformClick();
        }

        private void ToggleConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Hide = !Program.Hide;
        }

        private void CollectGarbageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Logger.Info("Collecting Garbage...");
            GC.Collect();
        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.ShowErrorMessage("Not implemented yet!");
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.Show();
        }

        private void ImportEventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.ShowErrorMessage("Not implemented yet!");
        }

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string text = Properties.Resources.help;
            InfoTextForm form = new InfoTextForm(text);
            form.ShowDialog();
        }

        private void RestartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestartApplication();
        }

        /* Notify Icon Context Menu Items*/

        private void NextWallpaperMenuItem_Click(object sender, EventArgs e)
        {
            NextWallpaper();
        }

        private void StartStopServiceMenuItem_Click(object sender, EventArgs e)
        {
            if (running)
                StopService();
            else
                StartService();
        }

        private void OpenStripMenuItem_Click(object sender, EventArgs e)
        {
            RecoverFromTray();
        }

        private void RestartToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RestartApplication();
        }

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            close = true;
            Program.Logger.Info("Close application");
            this.Close();
            Environment.Exit(0);
        }

        #endregion Tool Strip

        #endregion UI Controls
    }
}
