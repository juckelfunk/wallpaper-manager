﻿namespace Wallpaper_Manager
{
    partial class WallpaperEventButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.infoButton = new System.Windows.Forms.Button();
            this.playNowButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.activationButton = new System.Windows.Forms.Button();
            this.duplicateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(4, 4);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(200, 90);
            this.button.TabIndex = 0;
            this.button.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolTip.SetToolTip(this.button, "Edit Wallpaper Event");
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.Button_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Location = new System.Drawing.Point(210, 5);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(160, 90);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.PictureBox_Click);
            this.pictureBox.MouseLeave += new System.EventHandler(this.UnHover);
            this.pictureBox.MouseHover += new System.EventHandler(this.Hover);
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(405, 62);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(32, 32);
            this.removeButton.TabIndex = 3;
            this.removeButton.Text = "X";
            this.toolTip.SetToolTip(this.removeButton, "Delete Wallpaper Event");
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // infoButton
            // 
            this.infoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.infoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoButton.Location = new System.Drawing.Point(405, 4);
            this.infoButton.Name = "infoButton";
            this.infoButton.Size = new System.Drawing.Size(32, 32);
            this.infoButton.TabIndex = 1;
            this.infoButton.Text = "i";
            this.toolTip.SetToolTip(this.infoButton, "Show information about Wallpaper Event");
            this.infoButton.UseVisualStyleBackColor = true;
            this.infoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // playNowButton
            // 
            this.playNowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.playNowButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playNowButton.Location = new System.Drawing.Point(405, 33);
            this.playNowButton.Name = "playNowButton";
            this.playNowButton.Size = new System.Drawing.Size(32, 32);
            this.playNowButton.TabIndex = 2;
            this.playNowButton.Text = "!";
            this.toolTip.SetToolTip(this.playNowButton, "Run Wallpaper Event now");
            this.playNowButton.UseVisualStyleBackColor = true;
            this.playNowButton.Click += new System.EventHandler(this.PlayNowButton_Click);
            // 
            // activationButton
            // 
            this.activationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.activationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activationButton.Location = new System.Drawing.Point(376, 3);
            this.activationButton.Name = "activationButton";
            this.activationButton.Size = new System.Drawing.Size(32, 32);
            this.activationButton.TabIndex = 4;
            this.activationButton.Text = "✗";
            this.toolTip.SetToolTip(this.activationButton, "Deactivate event");
            this.activationButton.UseVisualStyleBackColor = true;
            this.activationButton.Click += new System.EventHandler(this.ActivationButton_Click);
            // 
            // duplicateButton
            // 
            this.duplicateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.duplicateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.duplicateButton.Location = new System.Drawing.Point(376, 63);
            this.duplicateButton.Name = "duplicateButton";
            this.duplicateButton.Size = new System.Drawing.Size(32, 32);
            this.duplicateButton.TabIndex = 5;
            this.duplicateButton.Text = "◨";
            this.toolTip.SetToolTip(this.duplicateButton, "Duplicate event");
            this.duplicateButton.UseVisualStyleBackColor = true;
            this.duplicateButton.Click += new System.EventHandler(this.DuplicateButton_Click);
            // 
            // WallpaperEventButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.duplicateButton);
            this.Controls.Add(this.activationButton);
            this.Controls.Add(this.playNowButton);
            this.Controls.Add(this.infoButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.button);
            this.Name = "WallpaperEventButton";
            this.Size = new System.Drawing.Size(443, 98);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button infoButton;
        public System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button playNowButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button activationButton;
        private System.Windows.Forms.Button duplicateButton;
    }
}
