﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

/*
 * Title: Settings Form
 * Author: Juckelfunk
 * Date: 01.07.2018 | 14:27 CET
 * Desc: Settings the user can change that affect the programs behaviour
 */

namespace Wallpaper_Manager
{
    public partial class SettingsForm : Form
    {
        public string ResultParameters { get; set; } // Parameters that signalize the MainForm to do something after the settings form has been shown

        // Settings the user has changed but are not applied yet
        private bool inStartup = false;
        private bool messageOnStartup = false;
        private bool messageOnTray = false;
        private bool previews = false;
        private bool currentPreviews = false;
        private bool logging = false;
        private string logPath = null;
        private bool consoleOnStart = false;
        private bool startLastEvent = false;
        private bool preferEventOverTime = false;
        private bool allowMultipleInstances = false;
        private bool askBeforeClosing = false;

        public SettingsForm()
        {
            InitializeComponent();
            ResultParameters = "";
            SetSettingsToUI();
        }

        /// <summary>
        /// Set the UI up according to the current settings
        /// </summary>
        private void SetSettingsToUI()
        {
            // Startup
            if (Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized"))
                startupLabel.Text = "Status: is in startup";
            inStartup = Settings.Default.InStartup;
            startupCheckBox.Checked = inStartup;

            // Message on startup
            messageOnStartup = Settings.Default.MessageOnStartup;
            messageOnStartupCheckbox.Checked = messageOnStartup;

            // Message on tray
            messageOnTray = Settings.Default.MesageOnTray;
            messageOnTrayCheckBox.Checked = messageOnTray;

            // Preview
            previews = Settings.Default.Previews;
            previewsCheckBox.Checked = previews;
            currentPreviews = previews;

            // Logging
            logging = Settings.Default.Logging;
            loggingCheckBox.Checked = logging;
            logPath = Settings.Default.LogPath;
            logPathTextBox.Text = logPath;

            // Console on start
            consoleOnStart = Settings.Default.ConsoleOnStart;
            consoleOnStartCheckBox.Checked = consoleOnStart;

            // Start last event
            startLastEvent = Settings.Default.StartLastEvent;
            startLastEventCheckBox.Checked = startLastEvent;

            // Prefer last event over time
            preferEventOverTime = Settings.Default.PreferEventOverTime;
            preferEventCheckBox.Checked = preferEventOverTime;

            // Allow multpiple instances
            allowMultipleInstances = Settings.Default.AllowMultipleInstances;
            allowMultipleInstancesCheckBox.Checked = allowMultipleInstances;

            // Ask to close
            askBeforeClosing = Settings.Default.AskBeforeClosing;
            askBeforeClosingCheckBox.Checked = askBeforeClosing;
        }

        /// <summary>
        /// Apply the startup settings to the registry
        /// </summary>
        /// <returns>Succsessfully applied to registry</returns>
        private bool ApplyStartup()
        {
            bool startupOK = true;
            // Should be in startup but is not
            if (inStartup && !Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized"))
                startupOK = Startup.RunOnStartup(Application.ProductName, Application.ExecutablePath + " --minimized");

            // Should not be in startup but is
            else if (!inStartup && Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized"))
                startupOK = Startup.RemoveFromStartup(Application.ProductName, Application.ExecutablePath + " --minimized");

            if (!startupOK)
            {
                string text = "Error while updateing the startup registry key!\nIn Startup: " +
                    Startup.IsInStartup(Application.ProductName, Application.ExecutablePath + " --minimized") +
                    "\nIf you have trouble with the startup you can manually delete / add the registry key. It should be in\n" +
                    @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" +
                    "\nSorry for the inconvinience :(";

                Program.Logger.Error(text);

                ConfirmForm form = new ConfirmForm(text, "Ignore", "Cancel");
                form.ShowDialog();

                // User cancels and may make changes
                if (form.Result == "Cancel")
                    return false;
            }
            Settings.Default.InStartup = inStartup;
            Settings.Default.ShouldStartup = inStartup;
            Settings.Default.Save();

            // Successfull or user does not care about error
            return true;
        }

        /// <summary>
        /// Apply changes to settings and exit with ok result
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            if (!ApplyStartup())
                return;

            if (!Directory.Exists(logPath))
            {
                Utils.ShowErrorMessage("Logging path '" + logPath + "' does not exist!");
                return;
            }
            
            // Apply Settings
            Settings.Default.MessageOnStartup = messageOnStartup;
            Settings.Default.MesageOnTray = messageOnTray;
            Settings.Default.Previews = previews;
            Settings.Default.LogPath = logPath;
            Settings.Default.Logging = logging;
            Settings.Default.ConsoleOnStart = consoleOnStart;
            Settings.Default.StartLastEvent = startLastEvent;
            Settings.Default.PreferEventOverTime = preferEventOverTime;
            Settings.Default.AllowMultipleInstances = allowMultipleInstances;
            Settings.Default.AllowMultipleInstancesUserPref = allowMultipleInstances;
            Settings.Default.AskBeforeClosing = askBeforeClosing;

            // If the preview setting has changes, the previews of the MainForm have to be reloaded
            if (Settings.Default.Previews != currentPreviews)
                ResultParameters += "RELOAD_PREVIEWS";

            Program.Logger.Info("Save settings:\n" + Utils.SettingsToString());
            Settings.Default.Save();

            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Do not apply changes and exit with cancel result
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Program.Logger.Info("Do not save settings");
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Reset all settings to default and apply that to the UI after user confirmation
        /// </summary>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Reset settings to default and save?", "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (res == DialogResult.Cancel)
                return;

            Settings.Default.Reset();
            Settings.Default.LogPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            Settings.Default.CurrentPath = Directory.GetCurrentDirectory() + @"\Events.txt";
            Settings.Default.Save();
            SetSettingsToUI();
            ApplyStartup();
        }

        private void StartupCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            inStartup = startupCheckBox.Checked;
        }

        private void MessageOnStartupCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            messageOnStartup = messageOnStartupCheckbox.Checked;
        }

        private void MessageOnTrayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            messageOnTray = messageOnTrayCheckBox.Checked;
        }

        private void PreviewsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            previews = previewsCheckBox.Checked;
        }

        private void ShowConsoleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            consoleOnStart = consoleOnStartCheckBox.Checked;
        }

        private void LoggingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            logging = loggingCheckBox.Checked;
        }

        private void LogPathTextBox_TextChanged(object sender, EventArgs e)
        {
            logPath = logPathTextBox.Text;
        }

        /// <summary>
        /// Let the user choose a directory in which the log files are created
        /// </summary>
        private void LogPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult res = dialog.ShowDialog();
            if (res == DialogResult.Cancel)
                return;
            if (String.IsNullOrEmpty(dialog.SelectedPath)
                || !Directory.Exists(dialog.SelectedPath))
            {
                Utils.ShowErrorMessage("Directory '" + dialog.SelectedPath + "' does not exist!");
                return;
            }
            logPath = dialog.SelectedPath;
            logPathTextBox.Text = logPath;
        }

        private void StartLastEventCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            startLastEvent = startLastEventCheckBox.Checked;
        }

        private void PreferEventCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            preferEventOverTime = preferEventCheckBox.Checked;
        }

        private void AllowMultipleInstancesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            allowMultipleInstances = allowMultipleInstancesCheckBox.Checked;
        }

        private void AskBeforeClosingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            askBeforeClosing = askBeforeClosingCheckBox.Checked;
        }

        /// <summary>
        /// Let the user confirm with Enter and exit with ESC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult res = MessageBox.Show("Save and exit to main window?", "Save", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Cancel)
                    return;
                okButton.PerformClick();
            }
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult res = MessageBox.Show("Do NOT save and exit to main window?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Cancel)
                    return;
                cancelButton.PerformClick();
            }
        }
    }
}
