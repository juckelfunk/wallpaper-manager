﻿namespace Wallpaper_Manager
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.startupLabel = new System.Windows.Forms.Label();
            this.startupCheckBox = new System.Windows.Forms.CheckBox();
            this.messageOnStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.startupGroupBox = new System.Windows.Forms.GroupBox();
            this.allowMultipleInstancesCheckBox = new System.Windows.Forms.CheckBox();
            this.miscellaniousGroupBox = new System.Windows.Forms.GroupBox();
            this.askBeforeClosingCheckBox = new System.Windows.Forms.CheckBox();
            this.preferEventCheckBox = new System.Windows.Forms.CheckBox();
            this.startLastEventCheckBox = new System.Windows.Forms.CheckBox();
            this.consoleOnStartCheckBox = new System.Windows.Forms.CheckBox();
            this.previewsCheckBox = new System.Windows.Forms.CheckBox();
            this.messageOnTrayCheckBox = new System.Windows.Forms.CheckBox();
            this.loggingGrouBox = new System.Windows.Forms.GroupBox();
            this.logPathButton = new System.Windows.Forms.Button();
            this.logPathTextBox = new System.Windows.Forms.TextBox();
            this.loggingCheckBox = new System.Windows.Forms.CheckBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.startupGroupBox.SuspendLayout();
            this.miscellaniousGroupBox.SuspendLayout();
            this.loggingGrouBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point(664, 502);
            this.okButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(112, 35);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "OK";
            this.toolTip.SetToolTip(this.okButton, "Save and exit");
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(543, 502);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.toolTip.SetToolTip(this.cancelButton, "DON\'T save an exit");
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // startupLabel
            // 
            this.startupLabel.AutoSize = true;
            this.startupLabel.Location = new System.Drawing.Point(207, 25);
            this.startupLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.startupLabel.Name = "startupLabel";
            this.startupLabel.Size = new System.Drawing.Size(159, 20);
            this.startupLabel.TabIndex = 2;
            this.startupLabel.Text = "Status: Not in startup";
            // 
            // startupCheckBox
            // 
            this.startupCheckBox.AutoSize = true;
            this.startupCheckBox.Location = new System.Drawing.Point(9, 29);
            this.startupCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.startupCheckBox.Name = "startupCheckBox";
            this.startupCheckBox.Size = new System.Drawing.Size(145, 24);
            this.startupCheckBox.TabIndex = 1;
            this.startupCheckBox.Text = "Start with Login";
            this.toolTip.SetToolTip(this.startupCheckBox, "Wallpaper Manager will start on the first login.\r\nTo do that Wallpaper Manager wi" +
        "ll create a registry key.");
            this.startupCheckBox.UseVisualStyleBackColor = true;
            this.startupCheckBox.CheckedChanged += new System.EventHandler(this.StartupCheckBox_CheckedChanged);
            // 
            // messageOnStartupCheckbox
            // 
            this.messageOnStartupCheckbox.AutoSize = true;
            this.messageOnStartupCheckbox.Location = new System.Drawing.Point(9, 65);
            this.messageOnStartupCheckbox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.messageOnStartupCheckbox.Name = "messageOnStartupCheckbox";
            this.messageOnStartupCheckbox.Size = new System.Drawing.Size(220, 24);
            this.messageOnStartupCheckbox.TabIndex = 2;
            this.messageOnStartupCheckbox.Text = "Show message on startup";
            this.toolTip.SetToolTip(this.messageOnStartupCheckbox, "Show message after Wallpaper Manager has started by startup");
            this.messageOnStartupCheckbox.UseVisualStyleBackColor = true;
            this.messageOnStartupCheckbox.CheckedChanged += new System.EventHandler(this.MessageOnStartupCheckbox_CheckedChanged);
            // 
            // startupGroupBox
            // 
            this.startupGroupBox.Controls.Add(this.allowMultipleInstancesCheckBox);
            this.startupGroupBox.Controls.Add(this.startupLabel);
            this.startupGroupBox.Controls.Add(this.messageOnStartupCheckbox);
            this.startupGroupBox.Controls.Add(this.startupCheckBox);
            this.startupGroupBox.Location = new System.Drawing.Point(18, 18);
            this.startupGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.startupGroupBox.Name = "startupGroupBox";
            this.startupGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.startupGroupBox.Size = new System.Drawing.Size(375, 137);
            this.startupGroupBox.TabIndex = 5;
            this.startupGroupBox.TabStop = false;
            this.startupGroupBox.Text = "Startup";
            // 
            // allowMultipleInstancesCheckBox
            // 
            this.allowMultipleInstancesCheckBox.AutoSize = true;
            this.allowMultipleInstancesCheckBox.Location = new System.Drawing.Point(9, 100);
            this.allowMultipleInstancesCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.allowMultipleInstancesCheckBox.Name = "allowMultipleInstancesCheckBox";
            this.allowMultipleInstancesCheckBox.Size = new System.Drawing.Size(202, 24);
            this.allowMultipleInstancesCheckBox.TabIndex = 3;
            this.allowMultipleInstancesCheckBox.Text = "Allow multiple instances";
            this.toolTip.SetToolTip(this.allowMultipleInstancesCheckBox, "Allow that multiple wallpaper managers are running at the same time (Recommended:" +
        " off)");
            this.allowMultipleInstancesCheckBox.UseVisualStyleBackColor = true;
            this.allowMultipleInstancesCheckBox.CheckedChanged += new System.EventHandler(this.AllowMultipleInstancesCheckBox_CheckedChanged);
            // 
            // miscellaniousGroupBox
            // 
            this.miscellaniousGroupBox.Controls.Add(this.askBeforeClosingCheckBox);
            this.miscellaniousGroupBox.Controls.Add(this.preferEventCheckBox);
            this.miscellaniousGroupBox.Controls.Add(this.startLastEventCheckBox);
            this.miscellaniousGroupBox.Controls.Add(this.consoleOnStartCheckBox);
            this.miscellaniousGroupBox.Controls.Add(this.previewsCheckBox);
            this.miscellaniousGroupBox.Controls.Add(this.messageOnTrayCheckBox);
            this.miscellaniousGroupBox.Location = new System.Drawing.Point(18, 165);
            this.miscellaniousGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.miscellaniousGroupBox.Name = "miscellaniousGroupBox";
            this.miscellaniousGroupBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.miscellaniousGroupBox.Size = new System.Drawing.Size(375, 243);
            this.miscellaniousGroupBox.TabIndex = 6;
            this.miscellaniousGroupBox.TabStop = false;
            this.miscellaniousGroupBox.Text = "Miscellanious";
            // 
            // askBeforeClosingCheckBox
            // 
            this.askBeforeClosingCheckBox.AutoSize = true;
            this.askBeforeClosingCheckBox.Location = new System.Drawing.Point(9, 206);
            this.askBeforeClosingCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.askBeforeClosingCheckBox.Name = "askBeforeClosingCheckBox";
            this.askBeforeClosingCheckBox.Size = new System.Drawing.Size(280, 24);
            this.askBeforeClosingCheckBox.TabIndex = 11;
            this.askBeforeClosingCheckBox.Text = "Ask to minimize rather than closing";
            this.toolTip.SetToolTip(this.askBeforeClosingCheckBox, "Ask if the user really wants to close the application or just minimize it");
            this.askBeforeClosingCheckBox.UseVisualStyleBackColor = true;
            this.askBeforeClosingCheckBox.CheckedChanged += new System.EventHandler(this.AskBeforeClosingCheckBox_CheckedChanged);
            // 
            // preferEventCheckBox
            // 
            this.preferEventCheckBox.AutoSize = true;
            this.preferEventCheckBox.Location = new System.Drawing.Point(38, 171);
            this.preferEventCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.preferEventCheckBox.Name = "preferEventCheckBox";
            this.preferEventCheckBox.Size = new System.Drawing.Size(189, 24);
            this.preferEventCheckBox.TabIndex = 10;
            this.preferEventCheckBox.Text = "Prefer event over time";
            this.toolTip.SetToolTip(this.preferEventCheckBox, "Start last event but check the events instead of time");
            this.preferEventCheckBox.UseVisualStyleBackColor = true;
            this.preferEventCheckBox.CheckedChanged += new System.EventHandler(this.PreferEventCheckBox_CheckedChanged);
            // 
            // startLastEventCheckBox
            // 
            this.startLastEventCheckBox.AutoSize = true;
            this.startLastEventCheckBox.Location = new System.Drawing.Point(9, 135);
            this.startLastEventCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.startLastEventCheckBox.Name = "startLastEventCheckBox";
            this.startLastEventCheckBox.Size = new System.Drawing.Size(253, 24);
            this.startLastEventCheckBox.TabIndex = 9;
            this.startLastEventCheckBox.Text = "Start last event on service start";
            this.toolTip.SetToolTip(this.startLastEventCheckBox, "Start the event that would have been triggered if the service would have run.");
            this.startLastEventCheckBox.UseVisualStyleBackColor = true;
            this.startLastEventCheckBox.CheckedChanged += new System.EventHandler(this.StartLastEventCheckBox_CheckedChanged);
            // 
            // consoleOnStartCheckBox
            // 
            this.consoleOnStartCheckBox.AutoSize = true;
            this.consoleOnStartCheckBox.Location = new System.Drawing.Point(9, 100);
            this.consoleOnStartCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.consoleOnStartCheckBox.Name = "consoleOnStartCheckBox";
            this.consoleOnStartCheckBox.Size = new System.Drawing.Size(305, 24);
            this.consoleOnStartCheckBox.TabIndex = 8;
            this.consoleOnStartCheckBox.Text = "Show console on start (not on startup)";
            this.toolTip.SetToolTip(this.consoleOnStartCheckBox, "Show debug console when Wallpaper Manager is started. (Toggle console with CTRL+T" +
        ")");
            this.consoleOnStartCheckBox.UseVisualStyleBackColor = true;
            this.consoleOnStartCheckBox.CheckedChanged += new System.EventHandler(this.ShowConsoleCheckBox_CheckedChanged);
            // 
            // previewsCheckBox
            // 
            this.previewsCheckBox.AutoSize = true;
            this.previewsCheckBox.Location = new System.Drawing.Point(9, 65);
            this.previewsCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.previewsCheckBox.Name = "previewsCheckBox";
            this.previewsCheckBox.Size = new System.Drawing.Size(301, 24);
            this.previewsCheckBox.TabIndex = 7;
            this.previewsCheckBox.Text = "Load small previews (Faster when off)";
            this.toolTip.SetToolTip(this.previewsCheckBox, "Load preview images. The loading times will be faster when turned off");
            this.previewsCheckBox.UseVisualStyleBackColor = true;
            this.previewsCheckBox.CheckedChanged += new System.EventHandler(this.PreviewsCheckBox_CheckedChanged);
            // 
            // messageOnTrayCheckBox
            // 
            this.messageOnTrayCheckBox.AutoSize = true;
            this.messageOnTrayCheckBox.Location = new System.Drawing.Point(9, 29);
            this.messageOnTrayCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.messageOnTrayCheckBox.Name = "messageOnTrayCheckBox";
            this.messageOnTrayCheckBox.Size = new System.Drawing.Size(207, 24);
            this.messageOnTrayCheckBox.TabIndex = 6;
            this.messageOnTrayCheckBox.Text = "Message when minimize";
            this.toolTip.SetToolTip(this.messageOnTrayCheckBox, "Show message when Wallpaper Manager minimizez to system tray");
            this.messageOnTrayCheckBox.UseVisualStyleBackColor = true;
            this.messageOnTrayCheckBox.CheckedChanged += new System.EventHandler(this.MessageOnTrayCheckBox_CheckedChanged);
            // 
            // loggingGrouBox
            // 
            this.loggingGrouBox.Controls.Add(this.logPathButton);
            this.loggingGrouBox.Controls.Add(this.logPathTextBox);
            this.loggingGrouBox.Controls.Add(this.loggingCheckBox);
            this.loggingGrouBox.Location = new System.Drawing.Point(402, 18);
            this.loggingGrouBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loggingGrouBox.Name = "loggingGrouBox";
            this.loggingGrouBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loggingGrouBox.Size = new System.Drawing.Size(375, 123);
            this.loggingGrouBox.TabIndex = 7;
            this.loggingGrouBox.TabStop = false;
            this.loggingGrouBox.Text = "Logging";
            // 
            // logPathButton
            // 
            this.logPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.logPathButton.Location = new System.Drawing.Point(254, 78);
            this.logPathButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.logPathButton.Name = "logPathButton";
            this.logPathButton.Size = new System.Drawing.Size(112, 35);
            this.logPathButton.TabIndex = 5;
            this.logPathButton.Text = "Set Folder";
            this.toolTip.SetToolTip(this.logPathButton, "Select the logging folder");
            this.logPathButton.UseVisualStyleBackColor = true;
            this.logPathButton.Click += new System.EventHandler(this.LogPathButton_Click);
            // 
            // logPathTextBox
            // 
            this.logPathTextBox.Location = new System.Drawing.Point(9, 78);
            this.logPathTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.logPathTextBox.Name = "logPathTextBox";
            this.logPathTextBox.Size = new System.Drawing.Size(234, 26);
            this.logPathTextBox.TabIndex = 4;
            this.toolTip.SetToolTip(this.logPathTextBox, "Directory in which the logging files will be created");
            this.logPathTextBox.TextChanged += new System.EventHandler(this.LogPathTextBox_TextChanged);
            // 
            // loggingCheckBox
            // 
            this.loggingCheckBox.AutoSize = true;
            this.loggingCheckBox.Location = new System.Drawing.Point(9, 29);
            this.loggingCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loggingCheckBox.Name = "loggingCheckBox";
            this.loggingCheckBox.Size = new System.Drawing.Size(146, 24);
            this.loggingCheckBox.TabIndex = 3;
            this.loggingCheckBox.Text = "Enable Logging";
            this.toolTip.SetToolTip(this.loggingCheckBox, "Log console output into logging file");
            this.loggingCheckBox.UseVisualStyleBackColor = true;
            this.loggingCheckBox.CheckedChanged += new System.EventHandler(this.LoggingCheckBox_CheckedChanged);
            // 
            // resetButton
            // 
            this.resetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.resetButton.Location = new System.Drawing.Point(18, 502);
            this.resetButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(112, 35);
            this.resetButton.TabIndex = 11;
            this.resetButton.Text = "Reset";
            this.toolTip.SetToolTip(this.resetButton, "Reset to defaults");
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(795, 555);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.loggingGrouBox);
            this.Controls.Add(this.miscellaniousGroupBox);
            this.Controls.Add(this.startupGroupBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings - Juckelfunks Wallpaper Manager";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SettingsForm_KeyDown);
            this.startupGroupBox.ResumeLayout(false);
            this.startupGroupBox.PerformLayout();
            this.miscellaniousGroupBox.ResumeLayout(false);
            this.miscellaniousGroupBox.PerformLayout();
            this.loggingGrouBox.ResumeLayout(false);
            this.loggingGrouBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label startupLabel;
        private System.Windows.Forms.CheckBox startupCheckBox;
        private System.Windows.Forms.CheckBox messageOnStartupCheckbox;
        private System.Windows.Forms.GroupBox startupGroupBox;
        private System.Windows.Forms.GroupBox miscellaniousGroupBox;
        private System.Windows.Forms.CheckBox previewsCheckBox;
        private System.Windows.Forms.CheckBox messageOnTrayCheckBox;
        private System.Windows.Forms.GroupBox loggingGrouBox;
        private System.Windows.Forms.Button logPathButton;
        private System.Windows.Forms.TextBox logPathTextBox;
        private System.Windows.Forms.CheckBox loggingCheckBox;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.CheckBox consoleOnStartCheckBox;
        private System.Windows.Forms.CheckBox startLastEventCheckBox;
        private System.Windows.Forms.CheckBox preferEventCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.CheckBox allowMultipleInstancesCheckBox;
        private System.Windows.Forms.CheckBox askBeforeClosingCheckBox;
    }
}