﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

/**
 * Title: Wallpaper Event
 * Author: Juckelfunk
 * Date: 23.05.2018 | 22:28
 * Desc: Represents a Wallpaper Event containing information about the time the wallpaper is changed and to what image it is changed
 */

namespace Wallpaper_Manager
{
    public class WallpaperEvent
    {
        // Events which can trigger this event
        public enum TriggerEvents
        {
            Nightmode_ON,
            Nightmode_OFF,
        }
        
        private bool random;                            // Should the next wallpaper be picked randomly or just the next one in the list
        private int innerChangeTime;                    // Time until the next wallpaper is shown if there are multiple wallpapers in this event
        private List<Wallpaper> alreadyShownWallpapers; // Already shown wallpapers will not be shown again if not canRepeatRandom
        private int index;                              // Index which wallpaper is currently set
        private Timer timer;                            // Timer which is used to trigger according to the innerChangeTime
        private bool activated;                         // The event will only be started when this is set to true (checked in the MainForm class)
        public bool running;                            // Set to true when event is started and set to false if stopped. Quiet obvious.

        public string Note { get; set; }                // 'Name' of this event. Has no purpose makes it easier for the user
        public DateTime Time { get; set; }              // Time on which this event will be triggered
        public TriggerEvents TriggerEvent { get; set; } // Event which will let this event trigger
        public bool UseTriggerEvent { get; set; }       // Should we be triggered by time or by an event
        public List<Wallpaper> Wallpapers { get; set; } // All wallpapers of this event
        public bool CanRepeatRandom { get; set; }       // If random, give the option to truly pick a random one, or first show all wallpaper that were not shown until now

        public bool Activated
        {
            get { return this.activated; }
            set
            {
                this.activated = value;
            }
        }

        public bool Random
        {
            get { return this.random; }
            set
            {
                this.random = value;
                SetUpTimer();
            }
        }
        public int InnerChangeTime
        {
            get { return this.innerChangeTime; }
            set
            {
                if (timer != null)
                    timer.Interval = value;
                this.innerChangeTime = value;
            }
        }

        /// <summary>
        /// A wallpape event that will be triggered by time or by an event
        /// </summary>
        /// <param name="note">Note that will help the user to indentify this event</param>
        /// <param name="time">Time on which this event will trigger</param>
        /// <param name="triggerEvent">Trigger event which will trigger this event</param>
        /// <param name="useTriggerEvent">Use the trigger event instead of the time</param>
        /// <param name="wallpapers">The wallpapers that are set by this event</param>
        /// <param name="random">Set the next wallpaper random</param>
        /// <param name="canRepeatRandom">Set next wallpaper completly random and let it NOT run through all other wallpapers before</param>
        /// <param name="innerChangeTime">The time until the next wallpaper is set, if this event has mutliple wallpapers</param>
        public WallpaperEvent(string note, DateTime time, TriggerEvents triggerEvent, bool useTriggerEvent, List<Wallpaper> wallpapers, bool random, bool canRepeatRandom, int innerChangeTime, bool activated)
        {
            Note = note;
            Time = time;
            TriggerEvent = triggerEvent;
            UseTriggerEvent = useTriggerEvent;
            Wallpapers = wallpapers;
            Random = random;
            CanRepeatRandom = canRepeatRandom;
            InnerChangeTime = innerChangeTime;
            alreadyShownWallpapers = new List<Wallpaper>();
            index = -1; // -1 so that the first time it will be the wallpaper at position 0
            Activated = activated;
        }

        /// <summary>
        /// Start the inner timer and set first wallpaper
        /// </summary>
        public void Start()
        {
            Program.Logger.Info("Start event '" + Note + "'");
            running = true;
            NextWallpaper();
            SetUpTimer();
        }

        /// <summary>
        /// Stop inner timer
        /// </summary>
        public void Stop()
        {
            Program.Logger.Info("Stop event '" + Note + "'");
            running = false;
            if (timer != null && timer.Enabled)
                timer.Stop();
        }

        /// <summary>
        /// Start the timer
        /// </summary>
        private void SetUpTimer()
        {
            if (Wallpapers.Count > 1 && InnerChangeTime != 0)
            {
                timer = new Timer();
                timer.Interval = InnerChangeTime;
                timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
                timer.Start();
            }
        }

        /// <summary>
        /// Called when timer is triggered.
        /// Will set the next wallpaper
        /// </summary>
        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            NextWallpaper();
        }

        /// <summary>
        /// Sets the next wallpaper according to the settings of this wallpaper event
        /// </summary>
        public void NextWallpaper()
        {
            List<Wallpaper> availableWallpapers = Wallpapers;

            // Not random, so just set the next wallpaper in the list
            if (!Random)
            {
                index++;
                // Reset to first position if end of list is reached
                if (index >= Wallpapers.Count)
                    index = 0;
            }
            // Randomly select the new wallpaper
            else
            {
                if (!CanRepeatRandom)
                {
                    // Generate a list with all wallpapers except the ones already shown
                    availableWallpapers = Wallpapers.Except(alreadyShownWallpapers).ToList();

                    // If all wallpapers were already shown reset the already shown wallpapers
                    if (availableWallpapers.Count == 0)
                    {
                        availableWallpapers = Wallpapers;
                        alreadyShownWallpapers.Clear();
                    }
                }
                
                // Set index to a random available wallpaper
                Random random = new Random();
                index = random.Next(0, availableWallpapers.Count);
                // Add this wallpaper to the already shown list because we don't want to show this one again
                alreadyShownWallpapers.Add(availableWallpapers[index]);
            }

            // Set wallpaper
            availableWallpapers[index].Set();
        }

        /// <summary>
        /// Returns a nicely formated string which represents this wallpaper event
        /// </summary>
        /// <returns>A nicely formated string which represents this wallpaper event</returns>
        public override string ToString()
        {
            string res = "";

            res += "Note: " + Note + "\n";
            res += "Time: " + Time + "\n";
            res += "Event: " + TriggerEvent + "\n";
            res += "UseTriggerEvent: " + UseTriggerEvent + "\n";
            res += "Random: " + Random + "\n";
            res += "CanRepeatRandom: " + CanRepeatRandom + "\n";
            res += "InnerChangeTime: " + InnerChangeTime + "\n";
            res += "Index: " + index + "\n";
            res += "Activated: " + Activated + "\n";
            res += "Wallpapers:\n";
            foreach (Wallpaper w in Wallpapers)
                res += "    " + w + "\n";

            return res;
        }
    }
}
